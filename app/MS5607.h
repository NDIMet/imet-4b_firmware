#ifndef __MS5607_H
#define __MS5607_H
// **************************************************************************
//
//      International Met Systems
//
//      iQ Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   MS5607.H
//
//      CONTENTS    :   Header file for MS5607 Pressure Sensor
//
// **************************************************************************

// *************************************************************************
// CONSTANTS
// *************************************************************************

// I2C Address << 1 with CSB high
//#define  I2C_MS5607_ID          0xEC

// I2C Address << 1 with CSB low
#define  I2C_MS5607_ID          0xEE

// Sensor State Machine
#define  MS5607_ST_OFFLINE      0
#define  MS5607_ST_START        1
#define  MS5607_ST_T_READ       2
#define  MS5607_ST_P_START      3
#define  MS5607_ST_P_READ       4
#define  MS5607_ST_CALC         5
#define  MS5607_ST_IDLE         6

#define  MS5607_CORR_A0         4.4166
#define  MS5607_CORR_A1         0.9939
#define  MS5607_CORR_A2         2.0e-6

#define  MS5607_BUF_SIZE        32

#define  MS5607_FILTER_P        0.15
#define  MS5607_FILTER_T        0.5

// *************************************************************************
// TYPES
// *************************************************************************
typedef struct
{
  //Pressure sensitivity
  uint16_t C1;
  //Pressure offset
  uint16_t C2;
  //Temperature coefficient of pressure sensitivity
  uint16_t C3;
  //Temperature coefficient of pressure offset
  uint16_t C4;
  //Reference temperature
  uint16_t C5;
  //Temperature coefficient of the temperature
  uint16_t C6;
} sMS5607_Calibration_Data;

typedef struct
{
  // Temperature A/D conversion count
  uint32_t TCount;
  // Pressure A/D conversion count
  uint32_t PCount;
  // Temperature A/D conversion count
  uint32_t FilteredT;
  // Pressure A/D conversion count
  uint32_t FilteredP;
  // Temperature * 100.0 C
  int32_t Temperature;
  // Pressure * 100.0 mbar
  int32_t Pressure;
} sMS5607_Sensor_Data;

typedef struct
{
  // I2C Channel
  I2C_TypeDef* I2Cx;
  // Sensor State
  uint8_t State;
  // Wait Channel
  uint8_t WaitChannel;
  // Communications Error Flag
  FlagStatus Flag_CommsFault;
  // Calibration Data
  sMS5607_Calibration_Data CalibrationData;
  int16_t BiasCorrection;
  // SensorData
  sMS5607_Sensor_Data SensorData;
  uint32_t CorrectedPressure;
} sMS5607_Sensor;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
extern sMS5607_Sensor PressureSensor;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
void MS5607_Init(sMS5607_Sensor* PressureStructure);
void MS5607_Reset(sMS5607_Sensor* PressureStructure);
void MS5607_Handler(sMS5607_Sensor* PressureStructure);

#endif
