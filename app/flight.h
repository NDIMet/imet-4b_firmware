#ifndef __FLIGHT_H
#define __FLIGHT_H
// **************************************************************************
//
//      International Met Systems
//
//      iQ Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   flight.h
//
//      CONTENTS    :   Header file for querying and updating the flight
//                      status of the radiosonde.
//
// **************************************************************************

// *************************************************************************
// CONSTANTS
// *************************************************************************
#define  FLIGHT_ST_INIT            0
#define  FLIGHT_ST_ACQUIRE         1
#define  FLIGHT_ST_RFL             2
#define  FLIGHT_ST_LAUNCHED        3
#define  FLIGHT_ST_DESCEND         4
#define  FLIGHT_ST_GROUND          5

#define  FLIGHT_BUF_SIZE           30
#define  FLIGHT_GOOD_THRESHOLD     5
#define  FLIGHT_ALT_THRESHOLD      1000
#define  FLIGHT_LOS_LEVEL2         1000
#define  FLIGHT_LOS_LEVEL3         10000
#define  FLIGHT_LOS_LEVEL4         200000

// *************************************************************************
// TYPES
// *************************************************************************
typedef struct
{
  int32_t X;
  int32_t Y;
  int32_t Z;
  int32_t Latitude;
  int32_t Longitude;
  int32_t HeightEES;
  int32_t HeightMSL;
} sLOCATION;

typedef struct
{
  uint8_t Status;
  uint16_t GoodDataCounter;
  int32_t AltitudeBuffer[FLIGHT_BUF_SIZE];
  uint16_t AltitudeIndex;
  uint32_t LineOfSite;
  FlagStatus Flag_BufferFull;
  sLOCATION LaunchLocation;
} sFLIGHT_STATUS;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
extern sFLIGHT_STATUS FlightStatus;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
void Flight_Init(sFLIGHT_STATUS* ptrStatus);
void Flight_StatusHandler(sFLIGHT_STATUS* ptrStatus);

#endif
