#ifndef __GRF5010_h__
#define __GRF5010_h__
// **************************************************************************
//
//      Developed by Safari Circuits for International Met Systems
//
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Billy Hopkins
//
//                  Safari Circuits
//					411 Washington Street
//                  Otsego, MI 49078
//					Ph : (269) 694-9471 x337
//
//					Intermet Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//                  Ph : (616) 971-1008
//
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   GRF5010.h
//
//      CONTENTS    :   Header File for communication and control of the Guerilla
//                      RF GRF5010 RF Power Amplifier
//
// **************************************************************************
// *************************************************************************
// INCLUDES
// *************************************************************************
#include "includes.h"

// *************************************************************************
// CONSTANTS
// *************************************************************************
// Serial Port Peripheral Configuration
#define HMC832A_SPI             SPI1
#define HMC832A_SPI_RCC         RCC_APB2Periph_SPI1

// Transmitter IO Hardware Configuration
#define HMC832A_SCK_PORT        GPIOB
#define HMC832A_SCK_RCC         RCC_AHBPeriph_GPIOB
#define HMC832A_SCK_PIN         GPIO_Pin_3
#define HMC832A_SCK_PIN_SRC     GPIO_PinSource3
#define HMC832A_SCK_AF          GPIO_AF_5

#define HMC832A_MISO_PORT       GPIOB
#define HMC832A_MISO_RCC        RCC_AHBPeriph_GPIOB
#define HMC832A_MISO_PIN        GPIO_Pin_4
#define HMC832A_MISO_PIN_SRC    GPIO_PinSource4
#define HMC832A_MISO_AF         GPIO_AF_5

#define HMC832A_MOSI_PORT       GPIOB
#define HMC832A_MOSI_RCC        RCC_AHBPeriph_GPIOB
#define HMC832A_MOSI_PIN        GPIO_Pin_5
#define HMC832A_MOSI_PIN_SRC    GPIO_PinSource5
#define HMC832A_MOSI_AF         GPIO_AF_5

#define HMC832A_CS_PORT         GPIOA
#define HMC832A_CS_PIN          GPIO_Pin_15
#define HMC832A_CS_RCC          RCC_AHBPeriph_GPIOA

// Amplifier IO Hardware Configuration
#define RFPA_VON_PORT          GPIOA
#define RFPA_VON_PIN           GPIO_Pin_11
#define RFPA_VON_RCC           RCC_AHBPeriph_GPIOA

// *************************************************************************
// TYPES
// *************************************************************************
typedef enum
{
  RFPA_PWR_OFF = 0,
  RFPA_PWR_1 = 1,
  RFPA_PWR_2 = 2,
  RFPA_PWR_3 = 3,
  RFPA_PWR_4 = 4,
} TypeDef_RFPA_PowerLevel;

typedef enum
{
 HMC832A_ST_OFF = 0,
 HMC832A_ST_ON = 1,
} TypeDef_HMC832A_State;

typedef enum
{
  CC115L_MF_2FSK = 0b000,
  CC115L_MF_GFSK = 0b001,
  CC115L_MF_OOK = 0b011,
  CC115L_MF_4FSK = 0b100
} TypeDef_CC115L_ModulationFormat;

typedef struct
{
  TypeDef_HMC832A_State State;
  TypeDef_RFPA_PowerLevel PowerLevel;
  FlagStatus Flag_AutoPowerLevel;
  uint8_t Deviation;
  FlagStatus Flag_Online;
} sHMC832A;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
extern sHMC832A Transmitter;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
void HMC832A_Init(sHMC832A* ptrTx);
void HMC832A_SetPowerAmpLevel(sHMC832A* ptrTx, TypeDef_RFPA_PowerLevel PowerLevel);
void HMC832A_SetPLLState(sHMC832A* ptrTx, TypeDef_HMC832A_State State);
void HMC832A_SetFrequency(sHMC832A* ptrTx, uint16_t Frequency);



#endif
