// **************************************************************************
//
//      International Met Systems
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
char string[100];
char variable[20];
char FirmwareVersion[8] = "4.07"; // 7/21/22 JHM
char SoftwareNumber[] = "612303"; // Update PN, 5/7/21 MDB
int Packet_Size = 0;
extern uint8_t iMet1_PTUX_Packet[IMET1_PTUX_SIZE];
uint8_t Initialized = FALSE;

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
// None.

// **************************************************************************
//
//  FUNCTION  : main
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Main routine for iMet-4 Radiosonde
//
//  UPDATED   : 2022-07-21 JHM - Changed GPS Settings
//
// **************************************************************************
int main(void)
{
  // Start Initialization
  Initialized = FALSE;

  // Get the saved configuration or set to default
  Config_Init();

  // Initialize the user interface
  UserInterface_Init();

  // Initialize the peripherals
  Peripherals_Init();

  // Start with all LEDs on to indicate initialization
  UserInterface_SetLED(&UserLEDs, LED_ALL, LED_ON);

  // Initialize manufacturing UART port
  Mfg_Init();

  // Send version
  Mfg_TransmitMessage("iMet-4B Radiosonde Firmware v");
  Mfg_TransmitMessage(FirmwareVersion);
  Mfg_TransmitMessage("\r\n");

  if (Flag_Config_Defaults == SET)
  {
    Mfg_TransmitMessage("Default flash configuration used.\r\n");
  }
  else
  {
    Mfg_TransmitMessage("Flash configuration loaded successfully.\r\n");
  }

  // Initialize Sensors
  MS5607_Init(&PressureSensor);
  NTC_Init(&Thermistor);
  HYT271_Init(&HumiditySensor);
  Battery_Init(&Battery);
  HVAC_Init(&Heater);
  Defroster_Init(&Defroster);
  DAC_Initialize();

  // Initialize Ublox CAM-M8Q
  Ublox_Init();

   if (UBX_Status == UBX_STATUS_OFFLINE)
   {
     UserInterface_SetLED(&UserLEDs, LED_ALL, LED_FAST_BLINK);
     while (1)
     {
       Mfg_Handler(&RX_Buffer);
     }
   }

  // Initialize transmission and packets
  HMC832A_Init(&Transmitter);

  // Make sure all the initialization data has been sent
  while (TX_Buffer.Start != TX_Buffer.End);

  // Initialize the modulation
  Modulation_Init();

  // Initialize the XData interface
  //XDATA_Init(&xData);

  // Initialize the packets
  Packets_Init(ModulationMode);

  // Enable the modulation, which starts the modulation clock
  Modulation_Enable();

  // Initialize the flight status
  Flight_Init(&FlightStatus);

  // Get to a new line on the console
  Mfg_TransmitMessage("READY\r\n");

  // LEDs Off to signal end of initialization
  UserInterface_SetLED(&UserLEDs, LED_NONE, LED_OFF);

  // Set the state to blink
  UserInterface_ChangeChannel(0);

  // Enable the LED
  UserInterface_EnableLED(&UserLEDs, ENABLE);

  // End Initialization
  Initialized = TRUE;

  // Main loop
  while (1)
  {

    // Handle thermistor
    NTC_Handler(&Thermistor);

    // Handle GPS
    Ublox_Handler();

    // Handle messages
    Mfg_Handler(&RX_Buffer);

    // Handle ADC conversions
    Peripherals_ADC1_Handler();
    MS5607_Handler(&PressureSensor);
    HYT271_Handler(&HumiditySensor);
    Battery_Handler(&Battery);
    HVAC_Handler(&Heater);
    Defroster_Handler(&Defroster);

    if (Flag_SOL_Ready == SET)
    {
      // Reset the flag
      Flag_SOL_Ready = RESET;

      if (ModulationMode == MODULATION_MODE_IMS)
      {
        // Increment the SOL buffer
        UBX_SOL_Buffer.Start = (UBX_SOL_Buffer.Start + 1) % UBX_SOL_BUF_SIZE;
      }
    }

    if (Flag_POSLLH_Ready == SET)
    {
      // Reset the flag since we are handling it
      Flag_POSLLH_Ready = RESET;

      if (ModulationMode == MODULATION_MODE_IMS)
      {
        // Build an iMet-1 packet
        Packet_Size = Packets_Build_iMet1();
        // Write the data to the modulation buffer
        //Modulation_iMet1_Send(iMet1_GPS_Packet, IMET1_GPS_SIZE);
        Modulation_iMet1_Send(iMet1_PTUX_Packet, Packet_Size);

        // Bell202 can transmit another 80 bytes at 1200 baud after PTU and GPS packets,
        // so start tracking amount of bytes sent each second
        /*xData.BytesLeft = XDATA_MAX_BYTES;
        
        while (xData.Start != xData.End)
        {
          if (xData.Packets[xData.Start].PacketSize <= xData.BytesLeft)
          {
            // Subtract out the amount of bytes to send
            xData.BytesLeft -= xData.Packets[xData.Start].PacketSize;
            // Send the XDATA to the transmitter modulation buffer
            Modulation_iMet1_Send(xData.Packets[xData.Start].Packet, xData.Packets[xData.Start].PacketSize );
            // Increment the XDATA buffer safely
            xData.Start = (xData.Start + 1) % XDATA_BUF_SIZE;
          } // if
          else
          {
            // We can't fit the next packet in this second, so escape the while loop
            break;
          }
        } // while*/

        // Increment packet number
        iMet1_PacketNumber++;
      }

      // Send an ASCII message if the mode is set
      Mfg_TransmitASCII();

      // Update the flight status
      //Flight_StatusHandler(&FlightStatus);

      // Increment the POSLLH buffer
      UBX_POSLLH_Buffer.Start = (UBX_POSLLH_Buffer.Start + 1) % UBX_POSLLH_BUF_SIZE;
    }// 1 second data transmission from POSLLH

    if (xData.Flag_XOFF)
    {
      if (XDATA_GetBufferSpace(&xData) > 1)
      {
        // Clear the XOFF flag
        xData.Flag_XOFF = RESET;
        // Send XON
        Mfg_SendByte(XON);
      }
    }
  } // while
} // main
