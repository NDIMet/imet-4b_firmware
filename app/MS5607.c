// **************************************************************************
//
//      International Met Systems
//
//      iQ Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   MS5607.C
//
//      CONTENTS    :   Routines for initialization and communication with the
//                      MS5607 Pressure Sensor (I2C Interface)
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

//Define exponents for clarity in calculations (ie EXP2_8 = 2^8)
#define  EXP2_8                 256
#define  EXP2_23                8388608
#define  EXP2_17                131072
#define  EXP2_6                 64
#define  EXP2_16                65536
#define  EXP2_7                 128
#define  EXP2_21                2097152
#define  EXP2_15                32768
#define  EXP2_31                2147483648
#define  EXP2_4                 16

// I2C Commands
#define  MS_RESET               0x1E
#define  CONV_D1_256            0x40
#define  CONV_D1_512            0x42
#define  CONV_D1_1024           0x44
#define  CONV_D1_2048           0x46
#define  CONV_D1_4096           0x48
#define  CONV_D2_256            0x50
#define  CONV_D2_512            0x52
#define  CONV_D2_1024           0x54
#define  CONV_D2_2048           0x56
#define  CONV_D2_4096           0x58
#define  ADC_READ               0x00
#define  READ_C1                0xA2
#define  READ_C2                0xA4
#define  READ_C3                0xA6
#define  READ_C4                0xA8
#define  READ_C5                0xAA
#define  READ_C6                0xAC

#define  MET_PRESSURE           1
#define  MET_TEMPERATURE        2

typedef uint8_t MetType;

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
sMS5607_Sensor PressureSensor;

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
// None.

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void WriteI2C(sMS5607_Sensor* ptrPressure, uint8_t Command);
static void ReadI2C(sMS5607_Sensor* ptrPressure, uint8_t* ptrData, uint8_t Length);
static void GetCalibrationCoefficients(sMS5607_Sensor* ptrPressure);
static void CalculateSensorData(sMS5607_Sensor* ptrPressure);
static void StartConversion(sMS5607_Sensor* ptrPressure, MetType Type);
static void GetConversion(sMS5607_Sensor* ptrPressure, MetType Type);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : WriteI2C
//
//  I/P       : sMS5607_Sensor* ptrPressure = pointer to a pressure data structure
//              uint8_t Command = the command to send via I2C bus
//
//  O/P       : None.
//
//  OPERATION : Sends the command over the I2C bus pointed to within the data
//              structure. If the command is not acknowledged by the sensor, it
//              sets the Flag_CommsFault flag for handling. This routine will
//              cause a hard fault if ptrPressure->I2Cx is null.
//
//  UPDATED   : 2016-04-13 JHM
//
// **************************************************************************
static void WriteI2C(sMS5607_Sensor* ptrPressure, uint8_t Command)
{
  // Clear NACK flag, so that we can check whether the device responds correctly
  I2C_ClearFlag(ptrPressure->I2Cx, I2C_FLAG_NACKF);

  // Use transfer handling to configure the I2C for operation
  I2C_TransferHandling(ptrPressure->I2Cx, I2C_MS5607_ID, 1, I2C_AutoEnd_Mode, I2C_Generate_Start_Write);

  while ((I2C_GetFlagStatus(ptrPressure->I2Cx, I2C_FLAG_TXIS) == RESET) &&
         (I2C_GetFlagStatus(ptrPressure->I2Cx, I2C_FLAG_NACKF) == RESET));

  if (I2C_GetFlagStatus(ptrPressure->I2Cx, I2C_FLAG_NACKF) == SET)
  {
    // Clear the NACK flag
    I2C_ClearFlag(ptrPressure->I2Cx, I2C_FLAG_NACKF);
    // Set the flag in the data structure to be handled later
    ptrPressure->Flag_CommsFault = SET;
    return;
  }
  else
  {
    // Send the data
    I2C_SendData(ptrPressure->I2Cx, Command);
    while (I2C_GetFlagStatus(ptrPressure->I2Cx, I2C_FLAG_STOPF) == RESET);
    I2C_ClearFlag(ptrPressure->I2Cx, I2C_FLAG_STOPF);
  }
} // WriteI2C

// **************************************************************************
//
//  FUNCTION  : ReadI2C
//
//  I/P       : sMS5607_Sensor* ptrPressure = pointer to a pressure data structure
//              uint8_t* ptrData = pointer to destination array
//              uint8_t Length = length of the data transfer
//
//  O/P       : None.
//
//  OPERATION : Configures and reads the I2C peripheral pointed to in ptrPressure.
//              The data is read into the array pointed to by ptrData.
//
//  UPDATED   : 2016-04-13 JHM
//
// **************************************************************************
static void ReadI2C(sMS5607_Sensor* ptrPressure, uint8_t* ptrData, uint8_t Length)
{
  int byte_counter;
  int i;

  // Instruct the slave device to start reading the given number of bytes.
  // I2C_AutoEnd_Mode will terminate the operation.
  I2C_TransferHandling(ptrPressure->I2Cx, I2C_MS5607_ID, Length, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);

  for (byte_counter=0; byte_counter < Length; byte_counter++)
  {
    // Quick and nasty way of getting a timeout where this code jams once in a blue moon.
    i = 1000;
    while ((I2C_GetFlagStatus(ptrPressure->I2Cx,I2C_FLAG_RXNE) == RESET) && (i>0))
    {
      i--;
    } // while

    if (i == 0)
    {
      ptrPressure->Flag_CommsFault = SET;
      return;
    }

    *ptrData = I2C_ReceiveData(ptrPressure->I2Cx);
    ptrData++;
  } // for

  // Wait for Stop Flag (automatically generated by I2C_TransferHandling)
  while (I2C_GetFlagStatus(ptrPressure->I2Cx,I2C_FLAG_STOPF) == RESET);

  // Clear STOPF flag
  I2C_ClearFlag(ptrPressure->I2Cx, I2C_FLAG_STOPF);
} // ReadI2C

// **************************************************************************
//
//  FUNCTION  : MS5607_GetCalibrationCoefficients
//
//  I/P       : sMS5607_Sensor* ptrPressure = pointer to a pressure data structure
//
//  O/P       : None.
//
//  OPERATION : Reads the calibration coefficients and stores them in the
//              pressure data structure for future calculations.
//
//  UPDATED   : 2016-04-13 JHM
//
// **************************************************************************
static void GetCalibrationCoefficients(sMS5607_Sensor* ptrPressure)
{
  unsigned char Data[2];

  // Coefficient 1
  WriteI2C(ptrPressure, READ_C1);
  ReadI2C(ptrPressure, Data, sizeof(Data));
  ptrPressure->CalibrationData.C1 = ((uint16_t)Data[0] << 8) | (uint16_t)Data[1];

  // Coefficient 2
  WriteI2C(ptrPressure, READ_C2);
  ReadI2C(ptrPressure, Data, sizeof(Data));
  ptrPressure->CalibrationData.C2 = ((uint16_t)Data[0] << 8) | (uint16_t)Data[1];

  // Coefficient 3
  WriteI2C(ptrPressure, READ_C3);
  ReadI2C(ptrPressure, Data, sizeof(Data));
  ptrPressure->CalibrationData.C3 = ((uint16_t)Data[0] << 8) | (uint16_t)Data[1];

  // Coefficient 4
  WriteI2C(ptrPressure, READ_C4);
  ReadI2C(ptrPressure, Data, sizeof(Data));
  ptrPressure->CalibrationData.C4 = ((uint16_t)Data[0] << 8) | (uint16_t)Data[1];

  // Coefficient 5
  WriteI2C(ptrPressure, READ_C5);
  ReadI2C(ptrPressure, Data, sizeof(Data));
  ptrPressure->CalibrationData.C5 = ((uint16_t)Data[0] << 8) | (uint16_t)Data[1];

  // Coefficient 6
  WriteI2C(ptrPressure, READ_C6);
  ReadI2C(ptrPressure, Data, sizeof(Data));
  ptrPressure->CalibrationData.C6 = ((uint16_t)Data[0] << 8) | (uint16_t)Data[1];
} // MS5607_GetCalibrationCoefficients

// **************************************************************************
//
//  FUNCTION  : CalculateSensorData
//
//  I/P       : sMS5607_Sensor* ptrPressure = pointer to a pressure data structure
//
//  O/P       : None.
//
//  OPERATION : Calculates the corrected pressure and temperature using the
//              counts and calibration data.
//
//  UPDATED   : 2016-04-13 JHM
//
// **************************************************************************
static void CalculateSensorData(sMS5607_Sensor* ptrPressure)
{
  uint32_t TCount, PCount;
  int32_t dT;
  int32_t Temp, Temp2;
  int64_t Offset, Sens, Offset2, Sens2;

  // Load the filtered counts
  TCount = ptrPressure->SensorData.FilteredT;
  PCount = ptrPressure->SensorData.FilteredP;

  // Difference between actual and reference temperature
  dT = TCount - (int32_t)ptrPressure->CalibrationData.C5 * EXP2_8;

  // Uncorrected Temperature
  Temp = 2000 + (int32_t)(((int64_t)dT *ptrPressure->CalibrationData.C6) / EXP2_23);

  // Offset at actual temperature
  Offset = ((int64_t)ptrPressure->CalibrationData.C2 * EXP2_17) + ((int64_t)ptrPressure->CalibrationData.C4 * dT) / EXP2_6;

  // Sensitivity at actual temperature
  Sens = (int64_t)ptrPressure->CalibrationData.C1 * EXP2_16 + ((int64_t)ptrPressure->CalibrationData.C3 * dT) / EXP2_7;
  
  // Corrected Temperature if below 20C (as described in data sheet)
  if(Temp < 2000)
  {
    Temp2 = (int32_t)(((int64_t)dT * dT) / EXP2_31);
    Offset2 = (int64_t)((61 * ((Temp - 2000) * (Temp - 2000))) / EXP2_4);
    Sens2 = (int64_t)(2 * (Temp - 2000) * (Temp - 2000));

    if (Temp < -1500)
    {
      Offset2 = Offset2 + (15 * (Temp + 1500) * (Temp + 1500));
      Sens2 = Sens2 + (8 * (Temp + 1500) * (Temp + 1500));
    }
  }
  else
  {
    Temp2 = 0;
    Offset2 = 0;
    Sens2 = 0;
  }

  // Compensate TEMP
  Temp -= Temp2;
  // Compensate Offset (OFF from data sheet)
  Offset -= Offset2;
  // Compensate Sensitivity (SENS from data sheet)
  Sens -= Sens2;

  // Move the temperature into the data structure
  ptrPressure->SensorData.Temperature = Temp;

  // Calculate the pressure
  ptrPressure->SensorData.Pressure = (int32_t)((((int64_t)PCount * Sens) / EXP2_21 - Offset) / EXP2_15);

  // Add in bias
  ptrPressure->SensorData.Pressure += ptrPressure->BiasCorrection;
} // CalculateSensorData

// **************************************************************************
//
//  FUNCTION  : StartConversion
//
//  I/P       : sMS5607_Sensor* ptrPressure = pointer to a pressure data structure
//              MetType Type = MET_PRESSURE or MET_TEMPERATURE
//
//  O/P       : None.
//
//  OPERATION : Sends the command over the I2C bus to immediately start a
//              conversion.
//
//  UPDATED   : 2016-04-13 JHM
//
// **************************************************************************
static void StartConversion(sMS5607_Sensor* ptrPressure, MetType Type)
{
  switch (Type)
  {
  case MET_PRESSURE:
    WriteI2C(ptrPressure, CONV_D1_4096);
    break;
  case MET_TEMPERATURE:
    WriteI2C(ptrPressure, CONV_D2_4096);
    break;
  default:
    break;
  } // switch
} // StartConversion

// **************************************************************************
//
//  FUNCTION  : GetConversion
//
//  I/P       : sMS5607_Sensor* ptrPressure = pointer to a pressure data structure
//              MetType Type = MET_PRESSURE or MET_TEMPERATURE
//
//  O/P       : None.
//
//  OPERATION : Gets the conversion value from the pressure sensor and stores
//              it into the data structure as PCount or TCount depending on
//              the MetType selected.
//
//  UPDATED   : 2016-04-13 JHM
//
// **************************************************************************
static void GetConversion(sMS5607_Sensor* ptrPressure, MetType Type)
{
  uint32_t ConversionValue;
  float fConversion, fFiltered, Value;
  uint8_t Data[3];

  // Send the command to read the ADC value
  WriteI2C(ptrPressure, ADC_READ);

  // Read the value
  ReadI2C(ptrPressure, Data, sizeof(Data));

  // Escape if there has been a communication error
  if (ptrPressure->Flag_CommsFault == SET)
  {
    return;
  }

  // Shift in conversion data
  ConversionValue = 0;
  ConversionValue |= ((uint32_t)Data[0]) << 16;
  ConversionValue |= ((uint32_t)Data[1]) << 8;
  ConversionValue |= ((uint32_t)Data[2]);

  // Move the data into the structure
  switch (Type)
  {
    case MET_PRESSURE:
      // Load the new count into the data structure
      ptrPressure->SensorData.PCount = ConversionValue;
      // Filter the value
      fConversion = (float)ConversionValue;
      fFiltered = (float)ptrPressure->SensorData.FilteredP;
      Value = fFiltered * (1.0 - MS5607_FILTER_P) + (fConversion * MS5607_FILTER_P);
      ptrPressure->SensorData.FilteredP = (uint32_t)Value;
      break;

    case MET_TEMPERATURE:
      // Load the new count into the data structure
      ptrPressure->SensorData.TCount = ConversionValue;
      // Filter the value
      fConversion = (float)ConversionValue;
      fFiltered = (float)ptrPressure->SensorData.FilteredT;
      Value = fFiltered * (1.0 - MS5607_FILTER_T) + (fConversion * MS5607_FILTER_T);
      ptrPressure->SensorData.FilteredT = (uint32_t)Value;
      break;

    default:
      break;
  } // switch
} // GetConversion

// **************************************************************************
//
//  FUNCTION  : CorrectPressure
//
//  I/P       : sPRESSURE_BUFFER* Buffer = Pointer to pressure buffer data structure
//
//  O/P       : None.
//
//  OPERATION : Uses the average pressure in the buffer to calculate the
//              corrected pressure. This corrects the sensor in accordance
//              with document 200901.0044.
//
//  UPDATED   : 2017-03-21 JHM
//
// **************************************************************************
static void CorrectPressure(sMS5607_Sensor* ptrPressure)
{
  double Pressure = (double)ptrPressure->SensorData.Pressure / 100.0;
  double CorrectedPressure = 0.0;

  CorrectedPressure += MS5607_CORR_A2 * Pressure * Pressure;
  CorrectedPressure += MS5607_CORR_A1 * Pressure;
  CorrectedPressure += MS5607_CORR_A0;

  ptrPressure->CorrectedPressure = (int32_t)(CorrectedPressure * 100.0);
} // CorrectPressure

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : MS5607_Init
//
//  I/P       : sMS5607_Sensor* ptrPressure = pointer to a pressure data structure
//
//  O/P       : None.
//
//  OPERATION : Initializes the pressure sensor. This resets the sensor, reads
//              the coefficients, and starts the state machine if the sensor
//              initialize properly.
//
//  UPDATED   : 2016-04-13 JHM
//
// **************************************************************************
void MS5607_Init(sMS5607_Sensor* ptrPressure)
{
  // Initialize values
  ptrPressure->I2Cx = PRESS_I2C;
  // Use the wait channel from peripherals.h
  ptrPressure->WaitChannel = PRESSURE_WAIT_CH;
  ptrPressure->State = MS5607_ST_OFFLINE;
  ptrPressure->Flag_CommsFault = RESET;

  ptrPressure->CalibrationData.C1 = 0;
  ptrPressure->CalibrationData.C2 = 0;
  ptrPressure->CalibrationData.C3 = 0;
  ptrPressure->CalibrationData.C4 = 0;
  ptrPressure->CalibrationData.C5 = 0;
  ptrPressure->CalibrationData.C6 = 0;

  ptrPressure->BiasCorrection = 0;

  ptrPressure->SensorData.TCount = 0;
  ptrPressure->SensorData.PCount = 0;
  ptrPressure->SensorData.FilteredT = 0;
  ptrPressure->SensorData.FilteredP = 0;

  ptrPressure->SensorData.Temperature = 0;
  ptrPressure->SensorData.Pressure = 0;

  // Indicate start of pressure sensor initialization
  Mfg_TransmitMessage("Initializing pressure sensor...");

  // Reset the sensor
  MS5607_Reset(ptrPressure);

  if (ptrPressure->Flag_CommsFault == SET)
  {
    ptrPressure->State = MS5607_ST_OFFLINE;
    Mfg_TransmitMessage("Offline.\r\n");
    return;
  }

  // Get Calibration Coefficients
  GetCalibrationCoefficients(ptrPressure);

  if (Flag_Config_Defaults == RESET)
  {
    ptrPressure->BiasCorrection = iQ_Config.PressureBiasCorrection;
  }

  // Start the machine
  ptrPressure->State = MS5607_ST_START;

  Mfg_TransmitMessage("Online.\r\n");
} // MS5607_Init

// **************************************************************************
//
//  FUNCTION  : MS5607_Reset
//
//  I/P       : sMS5607_Sensor* ptrPressure = pointer to a pressure data structure
//
//  O/P       : None.
//
//  OPERATION : Sends the I2C command that resets the pressure sensor.
//
//  UPDATED   : 2016-04-13 JHM
//
// **************************************************************************
void MS5607_Reset(sMS5607_Sensor* ptrPressure)
{
  int i;

  WriteI2C(ptrPressure, MS_RESET);

  if (ptrPressure->Flag_CommsFault == SET)
  {
    ptrPressure->State = MS5607_ST_OFFLINE;
  }
  else
  {
    Wait(ptrPressure->WaitChannel, 10);
    i = 0;
    while (GetWaitFlagStatus(ptrPressure->WaitChannel) == SET){i++;}
    ptrPressure->State = MS5607_ST_IDLE;
  }
} // MS5607_Reset

// **************************************************************************
//
//  FUNCTION  : MS5607_Handler
//
//  I/P       : sMS5607_Sensor* ptrPressure = pointer to a pressure data structure
//
//  O/P       : None.
//
//  OPERATION : Handles the state machine of the MS5607 pressure sensor.
//
//  UPDATED   : 16-04-13 JHM
//
// **************************************************************************
void MS5607_Handler(sMS5607_Sensor* ptrPressure)
{
  // Immediately return if the state machine is waiting
  if (GetWaitFlagStatus(ptrPressure->WaitChannel) == SET)
  {
    return;
  }

  if (ptrPressure->Flag_CommsFault == SET)
  {
    // Clear the flag
    ptrPressure->Flag_CommsFault = RESET;

    if (ptrPressure->State != MS5607_ST_OFFLINE)
    {
      // Reset the state machine if it is still online
      ptrPressure->State = MS5607_ST_START;
    }
  }

  switch (ptrPressure->State)
  {
    case MS5607_ST_IDLE:
      break;

    case MS5607_ST_START:
      // Advance state machine
      ptrPressure->State = MS5607_ST_T_READ;
      // Start the temperature measurement
      StartConversion(ptrPressure, MET_TEMPERATURE);
      Wait(ptrPressure->WaitChannel, 10);
      break;

    case MS5607_ST_T_READ:
      // Advance state machine
      ptrPressure->State = MS5607_ST_P_START;
      // Read the temperature measurement
      GetConversion(ptrPressure, MET_TEMPERATURE);
      Wait(ptrPressure->WaitChannel, 2);
      break;

    case MS5607_ST_P_START:
      // Advance state machine
      ptrPressure->State = MS5607_ST_P_READ;
      // Start the humidity measurement
      StartConversion(ptrPressure, MET_PRESSURE);
      Wait(ptrPressure->WaitChannel, 10);
      break;

    case MS5607_ST_P_READ:
      // Advance state machine
      ptrPressure->State = MS5607_ST_CALC;
      // Read the humidity measurement
      GetConversion(ptrPressure, MET_PRESSURE);
      break;

    case MS5607_ST_CALC:
      // Calculate and correct the pressure
      CalculateSensorData(ptrPressure);
      // Load the data into the buffer
      CorrectPressure(ptrPressure);
      // Reset the state machine
      ptrPressure->State = MS5607_ST_START;
      Wait(ptrPressure->WaitChannel, 2);
      break;

    default:
      break;
    } // switch (ptrPressure->State)
} // PressureHandler
