#ifndef __battery_h__
#define __battery_h__
// **************************************************************************
//
//      International Met Systems
//
//      iQ Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   battery.h
//
//      CONTENTS    :   Header file for battery voltage measurement
//
// **************************************************************************
// *************************************************************************
// INCLUDES
// *************************************************************************
#include "includes.h"

// *************************************************************************
// CONSTANTS
// *************************************************************************



// Buffer
#define BATT_BUF_SIZE          5

#define BATT_R1                2
#define BATT_R3                1

#define BATT_ST_OK             0
#define BATT_ST_LOW            1
#define BATT_ST_KILL           2

#define BATT_LOWBATTER_SP      3000
#define BATT_KILL_SP           2400

#define BATT_TICKER_THRESHOLD  100

// *************************************************************************
// TYPES
// *************************************************************************
typedef struct
{
  uint8_t State;
  uint16_t Index;
  uint16_t Counter;
  int16_t Counts[BATT_BUF_SIZE];
  FlagStatus Flag_ConversionReady;
  uint16_t milliVoltsADC;
  uint16_t milliVoltsBattery;
  FlagStatus Flag_LowBattery;
} sBATTERY;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
extern sBATTERY Battery;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
void Battery_Init(sBATTERY* ptrBattery);
void Battery_Handler(sBATTERY* ptrBattery);

#endif
