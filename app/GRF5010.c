// **************************************************************************
//
//      Developed by Safari Circuits for International Met Systems
//
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Billy Hopkins
//
//                  Safari Circuits
//					411 Washington Street
//                  Otsego, MI 49078
//					Ph : (269) 694-9471 x337
//
//					Intermet Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//                  Ph : (616) 971-1008
//
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   GRF5010.c
//
//      CONTENTS    :   Routines for communication and control of the Guerilla
//                      RF GRF5010 RF Power Amplifier
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        LOCAL CONSTANTS
//
// **************************************************************************
/*
// Command Strobe Register Locations
#define  SRES                 0x30
#define  SFSTXON              0x31
#define  SXOFF                0x32
#define  SCAL                 0x33
#define  STX                  0x35
#define  SIDLE                0x36
#define  SPWD                 0x39
#define  SFTX                 0x3B
#define  SNOP                 0x3D
*/

/*
 * HMC832A Configuration Register Locations
 */
uint8_t REG0[] = {0x00};
uint8_t REG1[] = {0x08};
uint8_t REG2[] = {0x10};
uint8_t REG3[] = {0x18};
uint8_t REG4[] = {0x20};
uint8_t REG5[] = {0x28};
uint8_t REG6[] = {0x30};
uint8_t REG7[] = {0x38};
uint8_t REG8[] = {0x40};
uint8_t REG9[] = {0x48};
uint8_t REGA[] = {0x50};
uint8_t REGB[] = {0x58};
uint8_t REGC[] = {0x60};
uint8_t REGD[] = {0x68};
uint8_t REGE[] = {0x70};
uint8_t REGF[] = {0x78};
uint8_t REG10[] = {0x80};
uint8_t REG11[] = {0x88};
uint8_t REG12[] = {0x90};
uint8_t REG13[] = {0x98};

/*
 * Default Register Values as loaded by PLL Eval Software
 */
uint8_t ZEROS[] = {0x00, 0x00, 0x00};
//Soft Reset for SPI. Write to Register 0 only
uint8_t REG0_SFTRST[] = {0x00, 0x00, 0x00};
uint8_t REG0_DEFAULT[] = {0x00, 0x00, 0x20};
//Turn PLL on. Write to Register 1 only
uint8_t REG1_PLLON[] = {0x00, 0x00, 0x02};



//Turn PLL off. Write to Register 1 only
uint8_t REG1_PLLOFF[] = {0x00, 0x00, 0x00};



//Reference Divider Value. Can be set between 0 and 2^14-1. Defaults to 0x01. Write to Register 2 only.
uint8_t REG2_RDIVIDER[] = {0x00, 0x00, 0x01};



//Frequency Register, Integer Part. Default to 0x21 for 1676 to 1682 MHz frequencies. Write toRegister 3 only.
uint8_t REG3_DEFAULT[] = {0x00, 0x00, 0x3E};



//Frequency Fractional Part. Defaults to 0x851E5B for 1676 MHX. Write to Register 4 only.
uint8_t REG4_1676[] = {0x13, 0xE9, 0x3F}; //1676.1
uint8_t REG4_1678[] = {0x26, 0xDF, 0xC3}; //1678.1
uint8_t REG4_1680[] = {0x39, 0xD6, 0x48}; //1680.1
uint8_t REG4_1682[] = {0x4C, 0xCC, 0xCD}; //1682.1



//VCO Gain/Divider Ratio. Defaults to 0x90 which is divide fo by 1. Write to Register 5 Only.
uint8_t REG5_1_DEFAULT[] = {0x00, 0x00, 0x90};



//VCO Mode. Defaults to 0xD98. Refer to Datasheet for specifics. Write to Register 5 only.
uint8_t REG5_2_DEFAULT[] = {0x00, 0x0F, 0x98};



//VCO Output Power. Defaults to 0x4B38. Refer to Datasheet for specifics. Write to Register 5 only
uint8_t REG5_3_DEFAULT[] = {0x00, 0x49, 0x38}; //power level 1
uint8_t REG5_3_PWRLVL2[] = {0x00, 0x4A, 0x38}; //power level 2
uint8_t REG5_3_PWRLVL3[] = {0x00, 0x4A, 0xB8}; //power level 3
uint8_t REG5_3_PWRLVL4[] = {0x00, 0x4C, 0x38}; //power level 4
uint8_t REG5_3_PWRLVL5[] = {0x00, 0x4C, 0xB8}; //power level 5
uint8_t REG5_3_PWRLVL6[] = {0x00, 0x4D, 0xB8}; //power level 6

uint8_t REG5_VCODISABLE[] = {0x00, 0xEF, 0x88};
uint8_t REG5_VCOENABLE[] = {0x00, 0xFF, 0x88};

//Sigma-Delta Configuration Register. Defaults to 0xF4A. Refer to Datasheet for specifics. Write to Register 6 only
uint8_t REG6_DEFAULT[] = {0x00, 0x0F, 0x4A};



//Lock Detect. Defaults to 0x14D. Refer to datasheet for specifics. Write to register 7 Only
uint8_t REG7_DEFAULT[] = {0x00, 0x01, 0x4D};



//Analog Enabling. Defaults to 0xC1BEFF. Refer to datasheet for specifics. Write to Register 8 Only.
uint8_t REG8_DEFAULT[] = {0xC1, 0xBE, 0xFF};



//Charge Pump Register. Defaults to 0x3FFEFD. Refer to dataheet for specifics. Write to Register 9 only.
uint8_t REG9_DEFAULT[] = {0x27, 0x5F, 0x3E};



//VCO Auto Cal Register. Defaults to 0x2046. Refer to datasheet for specifics. Write to Register A only.
uint8_t REGA_DEFAULT[3] = {0x00, 0x20, 0x46};



//Phase Detector Register. Defaults to 0x4F8061. Refer to Datasheet. Write to Register B Only.
uint8_t REGB_DEFAULT[3] = {0x4F, 0x80, 0x61};



//Exact Frequency Mode. Defaults to 0x0 which disbales the mode. Write to Register C only.
uint8_t REGC_DEFAULT[3] = {0x00, 0x00, 0x00};



//GPO,SPI, RDIV Register. Defaults to 0x81. Refer to the datasheet. Write to Register F only.
uint8_t REGF_DEFAULT[3] = {0x00, 0x00, 0x81};

// Miscellaneous bits
#define  SPI_READ_BIT         0x80
#define  SPI_BURST_BIT        0x40
#define  CHIP_RDYn_BIT        0x80
/*



*/

// *************************************************************************
//        LOCAL TYPES
// *************************************************************************
// None.

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
sHMC832A Transmitter;

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
// None.

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void InitRFPA(void);
static void InitSPI(void);
static void InitGPIO(void);
static void WriteRegister(uint8_t* Address, uint8_t* Data);
static uint8_t ReadRegister(uint8_t Address);
static void ManualReset(void);
static void ConfigureRegisters(void);
static void GetState(sHMC832A* ptrTx);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : InitRFPA
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the GPIO for the power amplifier
//
//  UPDATED   : 2016-05-03 JHM
//
// **************************************************************************
static void InitRFPA(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // Enable Clocks
  RCC_AHBPeriphClockCmd(RFPA_VON_RCC, ENABLE);

  // Pin configuration
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;

  // Amplifier control pins
  GPIO_InitStructure.GPIO_Pin = RFPA_VON_PIN;
  GPIO_Init(RFPA_VON_PORT, &GPIO_InitStructure);

  // Turn the amplifier off for initialization
    GPIO_SetBits(RFPA_VON_PORT, RFPA_VON_PIN);
} // InitRFPA

// **************************************************************************
//
//  FUNCTION  : InitSPI
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the SPI for the transmitter IC.
//
//  UPDATED   : 2016-05-02 JHM
//
// **************************************************************************
static void InitSPI(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  SPI_InitTypeDef SPI_InitStructure;

  // Enable SPI clock
  RCC_APB2PeriphClockCmd(HMC832A_SPI_RCC, ENABLE);


  // Configure SPI Pins
  // Map pins
  GPIO_PinAFConfig(HMC832A_SCK_PORT, HMC832A_SCK_PIN_SRC, HMC832A_SCK_AF);
  GPIO_PinAFConfig(HMC832A_MOSI_PORT, HMC832A_MOSI_PIN_SRC, HMC832A_MOSI_AF);
  GPIO_PinAFConfig(HMC832A_MISO_PORT, HMC832A_MISO_PIN_SRC, HMC832A_MISO_AF);

  // Configure port hardware
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;

  GPIO_InitStructure.GPIO_Pin = HMC832A_SCK_PIN;
  GPIO_Init(HMC832A_SCK_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = HMC832A_MOSI_PIN;
  GPIO_Init(HMC832A_MOSI_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = HMC832A_MISO_PIN;
  GPIO_Init(HMC832A_MISO_PORT, &GPIO_InitStructure);

  // Reset SPI peripheral to default values
  SPI_Cmd(HMC832A_SPI, DISABLE);
  SPI_I2S_DeInit(HMC832A_SPI);

  // Configure the SPI
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
  // Set the SPI clock to 48MHz / 256 = 187.5 kHz
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;
  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_InitStructure.SPI_CRCPolynomial = 7;
  // Initialize the data structure
  SPI_Init(HMC832A_SPI, &SPI_InitStructure);

  // Set the FIFO threshold
  SPI_RxFIFOThresholdConfig(HMC832A_SPI, SPI_RxFIFOThreshold_QF);
  SPI_NSSPulseModeCmd(SPI1, ENABLE);
  // Enable the SPI interface
  SPI_Cmd(HMC832A_SPI, ENABLE);


} // InitSPI

// **************************************************************************
//
//  FUNCTION  : InitGPIO
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the CS GPIO Pin.
//
//  UPDATED   : 2019-09-26  BRH
//
// **************************************************************************
static void InitGPIO(void)
{
  //Define the Struct
  GPIO_InitTypeDef GPIO_InitStructure;

  //Activate the CS clock
  RCC_AHBPeriphClockCmd(HMC832A_CS_RCC, ENABLE);
  //RCC_APB2PeriphClockCmd(HMC832A_SPI_RCC, ENABLE);


  //Configure the pin
  //GPIO_PinAFConfig(GPIOA, GPIO_PinSource15, GPIO_AF_5);

  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Pin = HMC832A_CS_PIN;

  //Pass the data to the GPIO Struct
  GPIO_Init(HMC832A_CS_PORT, &GPIO_InitStructure);
  //SPI_SSOutputCmd(HMC832A_SPI, ENABLE);
  //Initialize it high
  GPIO_SetBits(HMC832A_CS_PORT, HMC832A_CS_PIN);
}//InitGPIO
// **************************************************************************
//
//  FUNCTION  : WriteRegister
//
//  I/P       : uint8_t Address = 6-bit address
//              uint8_t Data = data byte to write
//
//  O/P       : uint8_t = status register value
//
//  OPERATION : Writes a byte of data to the 6-bit address using the SPI
//              interface and returns the status register value.
//
//  UPDATED   : 2016-04-18 JHM
//
// **************************************************************************
static void WriteRegister(uint8_t* Address, uint8_t* Data)
{
  uint8_t* Addressptr;
  uint8_t* Dataptr;;

  // Assign data to variables
  Addressptr = Address;
  Dataptr = Data;

  GPIO_ResetBits(HMC832A_CS_PORT, HMC832A_CS_PIN);
  // Send the first 8 bits
  SPI_SendData8(HMC832A_SPI, *Dataptr);
  Dataptr++;
  DelayMs(1);
  // Wait for the transfer to complete
  while (SPI_I2S_GetFlagStatus(HMC832A_SPI, SPI_I2S_FLAG_RXNE) == RESET);

  // Clear the data from the register by reading
  //SPI_ReceiveData8(HMC832A_SPI);


  // Send the next 8 bits of data
  SPI_SendData8(HMC832A_SPI, *Dataptr);
  Dataptr++;
  DelayMs(1);
  // Wait for the transfer to complete
  while (SPI_I2S_GetFlagStatus(HMC832A_SPI, SPI_I2S_FLAG_RXNE) == RESET);

  // Send the last 8 bits of data
  SPI_SendData8(HMC832A_SPI, *Dataptr);
  DelayMs(1);
  // Wait for the transfer to complete
  while (SPI_I2S_GetFlagStatus(HMC832A_SPI, SPI_I2S_FLAG_RXNE) == RESET);

  //Send the address
  SPI_SendData8(HMC832A_SPI, *Addressptr);
  DelayMs(1);
  // Wait at least 20 ns per data sheet -
  // I measured this time as approximately 2.5 microseconds, so we are ok

  // Set CSn high to disable the SPI
  //for(i=0; i<10; i++){}
  GPIO_SetBits(HMC832A_CS_PORT, HMC832A_CS_PIN);

  // Wait 1 ms
  DelayMs(5);

} // WriteRegister

// **************************************************************************
//
//  FUNCTION  : ReadRegister
//
//  I/P       : uint8_t Address = 6-bit address
//
//  O/P       : uint8_t = register data
//
//  OPERATION : Reads a single byte of data from the specified address using
//              the SPI interface
//
//  UPDATED   : 2016-04-18 JHM
//
// **************************************************************************
static uint8_t ReadRegister(uint8_t Address)
{
  uint8_t Header;
  uint8_t RegValue;

  // Move the address into the header packet
  Header = Address;

  // Set the read bit
  Header |= SPI_READ_BIT;

  // Clear burst bit. We will only send 1 byte at a time
  Header &= ~SPI_BURST_BIT;

  // Set CSn low to enable the SPI
  GPIO_ResetBits(HMC832A_CS_PORT, HMC832A_CS_PIN);

  // Wait at least 20 ns per data sheet -
  // I measured this time as approximately 6 microseconds, so we are ok

  // Send the header for the register
  SPI_SendData8(HMC832A_SPI, Header);

  // Wait for the transfer to complete
  while (SPI_I2S_GetFlagStatus(HMC832A_SPI, SPI_I2S_FLAG_RXNE) == RESET);

  // Receive the data (even though we wont use it)
  SPI_ReceiveData8(HMC832A_SPI);

  // Wait at least 55 ns per data sheet
  // I measured this time as approximately 5 microseconds, so we are ok

  // Clear the receive flag so we can use it
  SPI_I2S_ClearFlag(HMC832A_SPI, SPI_I2S_FLAG_RXNE);

  // Send null data since we are only reading
  SPI_SendData8(HMC832A_SPI, 0x00);

  // Wait for the transfer to complete
  while (SPI_I2S_GetFlagStatus(HMC832A_SPI, SPI_I2S_FLAG_RXNE) == RESET);

  // Read the last value in the SPI receive buffer. This should have been sent
  // while the data was transmitting
  RegValue = SPI_ReceiveData8(HMC832A_SPI);

  // Wait at least 20 ns per data sheet
  // I measured this time as approximately 2.5 microseconds, so we are ok

  // Set CSn high to disable the SPI
  GPIO_SetBits(HMC832A_CS_PORT, HMC832A_CS_PIN);

  // Wait 1 ms
  DelayMs(1);

  return RegValue;
}



// **************************************************************************
//
//  FUNCTION  : ManualReset
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Performs the power on startup sequence described in
//              "5.10.1.2 Manual Reset" on page 28 of the data sheet. Upon
//              completion, the chip will be in the IDLE state with XOSC and
//              the voltage regulator switched ON
//
//  UPDATED   : 2016-05-03 JHM
//
// **************************************************************************
static void ManualReset(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // Enable GPIO clocks
  RCC_AHBPeriphClockCmd(HMC832A_SCK_RCC, ENABLE);
  RCC_AHBPeriphClockCmd(HMC832A_MOSI_RCC, ENABLE);
  RCC_AHBPeriphClockCmd(HMC832A_MISO_RCC, ENABLE);
  RCC_AHBPeriphClockCmd(HMC832A_CS_RCC, ENABLE);

  // Disable the SPI interface since we will have to use these lines
  SPI_Cmd(HMC832A_SPI, DISABLE);
  SPI_I2S_DeInit(HMC832A_SPI);

  // Set SCLK, SI, and CSn as outputs
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  // SCLK
  GPIO_InitStructure.GPIO_Pin = HMC832A_SCK_PIN;
  GPIO_Init(HMC832A_SCK_PORT, &GPIO_InitStructure);
  // MOSI
  GPIO_InitStructure.GPIO_Pin = HMC832A_MOSI_PIN;
  GPIO_Init(HMC832A_SCK_PORT, &GPIO_InitStructure);
  // CSn
  GPIO_InitStructure.GPIO_Pin = HMC832A_CS_PIN;
  GPIO_Init(HMC832A_CS_PORT, &GPIO_InitStructure);

  // Set MISO to input
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_Pin = HMC832A_MISO_PIN;
  GPIO_Init(HMC832A_MISO_PORT, &GPIO_InitStructure);

  // Set outputs to initial state
  GPIO_SetBits(HMC832A_SCK_PORT, HMC832A_SCK_PIN);
  GPIO_ResetBits(HMC832A_MOSI_PORT, HMC832A_MOSI_PIN);
  GPIO_SetBits(HMC832A_CS_PORT, HMC832A_CS_PIN);

  // Delay 1 ms
  DelayMs(1);

  // Strobe CSn low to high
  GPIO_ResetBits(HMC832A_CS_PORT, HMC832A_CS_PIN);
  // Should I delay here?
  GPIO_SetBits(HMC832A_CS_PORT, HMC832A_CS_PIN);
  // Wait at least 40 us
  DelayMs(1);

  // Pull CSn low
  //GPIO_ResetBits(HMC832A_CS_PORT, HMC832A_CS_PIN);
  // Wait for MISO to go low
  //while (GPIO_ReadInputDataBit(HMC832A_MISO_PORT, HMC832A_MISO_PIN) == Bit_SET);

  // Reconfigure the pins for SPI
  InitSPI();

} // ManualReset

// **************************************************************************
//
//  FUNCTION  : ConfigureRegisters
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Configures the configuration registers for the TASK
//              transmission scheme. The center frequency will be set to the
//              value in the global Transmitter data structure.
//
//  UPDATED   : 2016-06-22 JHM
//
// **************************************************************************
static void ConfigureRegisters(void)
{
	//Write All the Registers to their default values
	WriteRegister(REG0, REG0_DEFAULT);
	WriteRegister(REG1, REG1_PLLON);
	WriteRegister(REG2, REG2_RDIVIDER);
	WriteRegister(REG5, REG5_1_DEFAULT);
	WriteRegister(REG5, REG5_2_DEFAULT);
	WriteRegister(REG5, REG5_3_PWRLVL6);
	WriteRegister(REG5, ZEROS);
	WriteRegister(REG6, REG6_DEFAULT);
	WriteRegister(REG7, REG7_DEFAULT);
	WriteRegister(REG8, REG8_DEFAULT);
	WriteRegister(REG9, REG9_DEFAULT);
	WriteRegister(REGA, REGA_DEFAULT);
	WriteRegister(REGB, REGB_DEFAULT);
	WriteRegister(REGC, REGC_DEFAULT);
	WriteRegister(REGF, REGF_DEFAULT);
	WriteRegister(REG3, REG3_DEFAULT);
	WriteRegister(REG4, REG4_1676);
} // ConfigureRegisters

// **************************************************************************
//
//  FUNCTION  : GetState
//
//  I/P       : sHMC832A* = Pointer to CC115L data structure
//
//  O/P       : None.
//
//  OPERATION : This routine strobes the NOP command and uses the status
//              register determine the state of the IC. Once the state is
//              determined it is immediately loaded into the data structure
//              input by the user.
//
//  UPDATED   : 2016-05-16 JHM
//
// **************************************************************************
static void GetState(sHMC832A* ptrTx)
{

} // GetState

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : CC115L_Init
//
//  I/P       : sHMC832A* = Pointer to CC115L data structure
//
//  O/P       : None.
//
//  OPERATION : Initializes the transmitter data structure and hardware.
//
//  UPDATED   : 2016-04-15 JHM
//
// **************************************************************************
void HMC832A_Init(sHMC832A* ptrTx)
{
  // Initialize some of the structure values
  //ptrTx->Flag_Online = RESET;

  // Initialize the RFPA GPIO
  InitGPIO();
  InitRFPA();
  InitSPI();

  //ManualReset();

  // Reset the transmitter to a known state
  //ManualReset();

  // Exit initialization if the manual reset has failed. Chip is offline.
  /*if (ptrTx->Flag_Online == RESET)
  {
    Mfg_TransmitMessage("Transmitter and PLL offline.\r\n");
    HMC832A_SetPLLState(ptrTx, RESET);
    HMC832A_SetPowerAmpLevel(ptrTx, RFPA_PWR_OFF);
    return;
  }*/

  // Configure the transmitter default settings
  ConfigureRegisters();
  while(!GPIO_ReadInputDataBit(HMC832A_MISO_PORT, HMC832A_MISO_PIN));
  Transmitter.Flag_Online = SET;
  Transmitter.State = HMC832A_ST_ON;

  // Set the configurable information
  //HMC832A_SetFrequency(ptrTx, FrequencyChannels.Frequencies[FrequencyChannels.Channel]);
  //HMC832A_SetDeviation(ptrTx, (uint8_t)iQ_Config.Deviation);
  //HMC832A_SetPLLState(ptrTx, (FlagStatus)iQ_Config.Flag_AutoPowerLevel);
  //HMC832A_SetPowerAmpLevel(ptrTx, (TypeDef_RFPA_PowerLevel)iQ_Config.PowerLevel);

  // Turn the transmitter on
  //HMC832A_SetState(ptrTx, ENABLE);

  // Notify the user
  Mfg_TransmitMessage("Transmitter and PLL online and enabled.\r\n");
  //GetState(ptrTx);
} // CC115L_Init



// **************************************************************************
//
//  FUNCTION  : HMC832A_SetPowerAmpLevel
//
//  I/P       :   sHMC832A* = Pointer to CC115L data structure
//                TypeDef_RFPA_PowerLevel :
//                  RFPA_PWR_OFF = IC and RFPA off
//                  RFPA_PWR_1 = ~5 dBm
//                  RFPA_PWR_2 = ~16 dBm
//                  RFPA_PWR_3 = ~23 dBm
//                  RFPA_PWR_4 = ~28 dBm
//
//  O/P       : None.
//
//  OPERATION : Sets the programmable amplifier power level using the IO pins.
//
//  UPDATED   : 2016-05-13 JHM
//
// **************************************************************************
void HMC832A_SetPowerAmpLevel(sHMC832A* ptrTx, TypeDef_RFPA_PowerLevel PowerLevel)
{
  if (PowerLevel == RFPA_PWR_OFF)
  {
    // Drive all the RFPA bits low
    GPIO_ResetBits(RFPA_VON_PORT, RFPA_VON_PIN);
  } // if
  else
  {
    // Enable the RFPA
    GPIO_SetBits(RFPA_VON_PORT, RFPA_VON_PIN);

    switch (PowerLevel)
    {
      case RFPA_PWR_OFF:
    	 Transmitter.State = HMC832A_ST_OFF;
    	 WriteRegister(REG1, REG1_PLLOFF);
    	 break;

      case RFPA_PWR_1:
    	  WriteRegister(REGC, REGC_DEFAULT);
    	  WriteRegister(REG5, REG5_3_DEFAULT);
    	  WriteRegister(REGC, REGC_DEFAULT);
        break;

      case RFPA_PWR_2:
    	  WriteRegister(REGC, REGC_DEFAULT);
    	  WriteRegister(REG5, REG5_3_PWRLVL3);
    	  WriteRegister(REGC, REGC_DEFAULT);
        break;

      case RFPA_PWR_3:
    	  WriteRegister(REGC, REGC_DEFAULT);
    	  WriteRegister(REG5, REG5_3_PWRLVL4);
    	  WriteRegister(REGC, REGC_DEFAULT);
        break;

      case RFPA_PWR_4:
    	  WriteRegister(REGC, REGC_DEFAULT);
    	  WriteRegister(REG5, REG5_3_PWRLVL6);
    	  WriteRegister(REGC, REGC_DEFAULT);
        break;

      default:
        break;
    } // switch
  } // else

  // Update the data structure
  ptrTx->PowerLevel = PowerLevel;
} // CC115L_SetPowerAmpLevel

// **************************************************************************
//
//  FUNCTION  : CC115L_SetAutoLevel
//
//  I/P       :   sHMC832A* = Pointer to CC115L data structure
//                FlagStatus AutoLevelOn
//                      SET = The power level is automatically controlled by
//                            flight status
//                      RESET = The power level is forced to the current
//                              power level.
//
//  O/P       : None.
//
//  OPERATION : Turn the automatic power level function on or off.
//
//  UPDATED   : 2016-10-14 JHM
//
// **************************************************************************
void HMC832A_SetPLLState(sHMC832A* ptrTx, TypeDef_HMC832A_State State)
{
  if (State)
  {
 	WriteRegister(REG5, REG5_VCOENABLE);
 	while(!GPIO_ReadInputDataBit(HMC832A_MISO_PORT, HMC832A_MISO_PIN));
    Transmitter.State = HMC832A_ST_ON;
    ptrTx->Flag_Online = SET;
  }
  else
  {
	WriteRegister(REG5, REG5_VCODISABLE);
	DelayMs(1);
	Transmitter.State = HMC832A_ST_OFF;
    ptrTx->Flag_Online = RESET;
  }
} // CC115L_SetAutoLevel

// **************************************************************************
//
//  FUNCTION  : CC115L_SetFrequency
//
//  I/P       : sHMC832A* = Pointer to CC115L data structure
//              uint16_t Frequency = 4020 to 4060 in tenths of MegaHertz
//
//  O/P       : None.
//
//  OPERATION : Sets the transmitter to transmit at the frequency specified.
//              In order for this method to function properly, the transmitter
//              must be in the IDLE state. Use CC115L_SetStatoe() prior to
//              using this function.
//
//  UPDATED   : 2016-10-14 JHM
//
// **************************************************************************
void HMC832A_SetFrequency(sHMC832A* ptrTx, uint16_t Frequency)
{
	//Disable the modulation so we aren't sending the data while we are screwing with the PLL
	Modulation_Disable();


	switch(Frequency)
	{
	 	 case 1676:
	 		//Write to the two registers for updating the frequencies
	 		 WriteRegister(REG3, REG3_DEFAULT);
	 		 WriteRegister(REG4, REG4_1676);
	 		 HMC832A_SetPowerAmpLevel(&Transmitter, RFPA_PWR_4);
	 		 //Wait for the PLL to acheive lock.
	 		 while(!GPIO_ReadInputDataBit(HMC832A_MISO_PORT, HMC832A_MISO_PIN));
	 		 break;

	 	 case 1678:
	 		WriteRegister(REG3, REG3_DEFAULT);
	 		WriteRegister(REG4, REG4_1678);
	 		HMC832A_SetPowerAmpLevel(&Transmitter, RFPA_PWR_4);
	 		while(!GPIO_ReadInputDataBit(HMC832A_MISO_PORT, HMC832A_MISO_PIN));
	 		break;

	 	 case 1680:
	 		WriteRegister(REG3, REG3_DEFAULT);
	 		WriteRegister(REG4, REG4_1680);
	 		HMC832A_SetPowerAmpLevel(&Transmitter, RFPA_PWR_4);
	 		while(!GPIO_ReadInputDataBit(HMC832A_MISO_PORT, HMC832A_MISO_PIN));
	 		break;

	 	 case 1682:
	 		WriteRegister(REG3, REG3_DEFAULT);
	 		WriteRegister(REG4, REG4_1682);
	 		HMC832A_SetPowerAmpLevel(&Transmitter, RFPA_PWR_4);

	 		while(!GPIO_ReadInputDataBit(HMC832A_MISO_PORT, HMC832A_MISO_PIN));
	 		break;

	 	 default:
	 		 Mfg_TransmitMessage("ERR03");
	 		 return;
	}

	//Re-Enable the Modulation.
	Modulation_Enable();

} // HMC832A_SetFrequency




