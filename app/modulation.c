// **************************************************************************
//
//      International Met Systems
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   modulation.c
//
//      CONTENTS    :   Routines for communication and control of modulated
//                      data output
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        CONSTANTS
//
// **************************************************************************

// **************************************************************************
//
//        TYPES
//
// **************************************************************************
// None.

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
FlagStatus Flag_Build_TASK = RESET;
TypeDef_ModulationMode ModulationMode = MODULATION_MODE_IMS;
siMet1Modulation iMet1Modulation;
extern uint16_t Baud_Capture;
// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
static uint32_t FM0InterruptCounter = 0;
static uint32_t FM0ByteNumber = 0;
static uint32_t FM0Byte = 0;
static uint8_t FM0Bit = 0x80;
// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void InitGPIO(void);
static void InitTaskTimer(void);
static void InitiMet1Timer(void);
static void ResetBuffer(siMet1Modulation* ptrModulation);
int Tx_Buffer_SpaceAvail();
// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// *************************************************************************
//
//  FUNCTION  : InitGPIO
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the GPIO for the modulation data pin.
//
//  UPDATED   : 2016-04-06 JHM
//
// *************************************************************************
static void InitGPIO(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // Enable the clock for GPIOX
  RCC_AHBPeriphClockCmd(MOD0_GPIO_RCC, ENABLE);

  // Configure the LED output pin(s)
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_2;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
  GPIO_InitStructure.GPIO_Pin = MOD0_PIN;
  GPIO_Init(MOD0_PORT, &GPIO_InitStructure);

  // Start with the pin low
  GPIO_ResetBits(MOD0_PORT, MOD0_PIN);
} // InitGPIO

// *************************************************************************
//
//  FUNCTION  : InitTaskTimer
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the timer for the modulation data. No prescaler
//              is used in an attempt to make the modulation clock as
//              accurate as possible. Based on the calculation:
//              N = 11719
//              SYSCLK = 48000000
//              TIMCLK = SYSCLK / N = 4095.91262 Hz
//
//  UPDATED   : 2016-04-08 JHM
//
// *************************************************************************
static void InitTaskTimer(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

  // Reset the timer peripheral to defaults
  TIM_DeInit(MOD0_TIM);

  // TIMX clock enable
  RCC_APB1PeriphClockCmd(MOD0_TIM_RCC, ENABLE);

  // Enable the TIMX global Interrupt
  NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  // Set the interrupt priority to highest
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_Init(&NVIC_InitStructure);

  TIM_TimeBaseStructure.TIM_Prescaler = 0;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_Period = 11719 - 1;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseInit(MOD0_TIM, &TIM_TimeBaseStructure);

  // Start with the interrupts disabled
  TIM_ITConfig(MOD0_TIM, TIM_IT_Update, DISABLE);

  // Start with the timer disabled
  TIM_Cmd(MOD0_TIM, DISABLE);
} // InitTimer

// *************************************************************************
//
//  FUNCTION  : InitiMet1Timer
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION :
//
//  UPDATED   : 2016-07-21 JHM
//
// *************************************************************************
static void InitiMet1Timer(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

  /*
   * Initialize the timer for the Baud Rate
   */

  // Disable the timer peripheral
  TIM_Cmd(IMET1_TIM, DISABLE);

  // Reset the timer peripheral to defaults
  TIM_DeInit(IMET1_TIM);

  // Enable the clock for the timer
  RCC_APB1PeriphClockCmd(IMET1_TIM_RCC, ENABLE);

  // Enable the TIMX global Interrupt
  NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  // Set the interrupt priority to highest
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_Init(&NVIC_InitStructure);

  // Set the timer clock to 264 kHz
  TIM_TimeBaseStructure.TIM_Prescaler = (uint16_t)(SystemCoreClock / IMET1_TIM_CLK) - 1;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  // Set period to maximum value
  TIM_TimeBaseStructure.TIM_Period = 0xFFFF;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseInit(IMET1_TIM, &TIM_TimeBaseStructure);

  // Start with the interrupts disabled
  TIM_ITConfig(IMET1_TIM, TIM_IT_CC1 | TIM_IT_CC2, DISABLE);


  // Start with the timer disabled
  TIM_Cmd(IMET1_TIM, DISABLE);
} // InitiMet1Timer

// *************************************************************************
//
//  FUNCTION  : ResetBuffer
//
//  I/P       : siMet1Modulation* ptrModulation = Pointer to iMet-1 modulation
//                                                data structure
//
//  O/P       : None.
//
//  OPERATION : Resets the values and buffer for iMet-1 modulation
//
//  UPDATED   : 2016-07-21 JHM
//
// *************************************************************************
static void ResetBuffer(siMet1Modulation* ptrModulation)
{
  int i;

  ptrModulation->Start = 0;
  ptrModulation->End = 0;
  ptrModulation->BitCounter = 0;
  ptrModulation->CCR_Value = IMET1_NULL_CCR_VAL;

  for (i = 0; i < IMET1_BUF_SIZE; i++)
  {
    ptrModulation->Buffer[i] = 0;
  }
} // ResetBuffer

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : Modulation_Init
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the modulation hardware and data.
//
//  UPDATED   : 2016-04-06 JHM
//
// **************************************************************************
void Modulation_Init(void)
{
  // Initialize the GPIO
  InitGPIO();

  // Get the modulation mode from config
  ModulationMode = (TypeDef_ModulationMode)iQ_Default.ModulationMode;

  if (ModulationMode == MODULATION_MODE_IMS)
  {
    // Reset the buffer
    ResetBuffer(&iMet1Modulation);

    // Initialize the iMet-1 timer peripheral
    InitiMet1Timer();
  }
  else
  {
    Modulation_Disable();
  }
} // Modulation_Init

// **************************************************************************
//
//  FUNCTION  : Modulation_Enable
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Enables the modulation interrupt. This routine should not be
//              called until the data buffer is full.
//
//  UPDATED   : 2016-04-08 JHM
//
// **************************************************************************
void Modulation_Enable(void)
{

  if (ModulationMode == MODULATION_MODE_IMS)
  {
    // Disable the clock to reset to zero
    TIM_Cmd(IMET1_TIM, DISABLE);

    // Reset the clock
    TIM_SetCounter(IMET1_TIM, 0);

    // Set the CCR values
    TIM_SetCompare1(IMET1_TIM, IMET1_BAUD_CCR_VAL);
    TIM_SetCompare2(IMET1_TIM, IMET1_NULL_CCR_VAL);

    // Enable the CCR interrupts
    TIM_ITConfig(IMET1_TIM, TIM_IT_CC1 | TIM_IT_CC2, ENABLE);

    // Enable the timer
    TIM_Cmd(IMET1_TIM, ENABLE);
  }
} // Modulation_Enable

// **************************************************************************
//
//  FUNCTION  : Modulation_Disable
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Disables the modulation interrupt
//
//  UPDATED   : 2017-04-10 JHM
//
// **************************************************************************
void Modulation_Disable(void)
{
  // Disable the Update interrupt for TASK mode if it is enabled
  if (MOD0_TIM->DIER & TIM_DIER_UIE)
  {
    TIM_ITConfig(MOD0_TIM, TIM_IT_Update, DISABLE);
  }

  // Disable the CCR interrupts for iMet-4 mode if they are enabled
  if (MOD0_TIM->DIER & (TIM_DIER_CC1IE | TIM_DIER_CC2IE))
  {
    TIM_ITConfig(IMET1_TIM, TIM_IT_CC1 | TIM_IT_CC2, DISABLE);
  }

  // Disable both timers if they are enabled
  if (MOD0_TIM->CR1 & TIM_CR1_CEN)
  {
    TIM_Cmd(MOD0_TIM, DISABLE);
  }
  if (IMET1_TIM->CR1 & TIM_CR1_CEN)
  {
    TIM_Cmd(IMET1_TIM, DISABLE);
  }
} // Modulation_Disable

// **************************************************************************
//
//  FUNCTION  : Modulation_TASK_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : This routine should be called each time the modulation data
//              interrupt occurs.
//
//  UPDATED   : 2016-04-06 JHM
//
// **************************************************************************
void Modulation_TASK_Handler(void)
{
  // Test frequency - this should be 4096 / 2 = 2048 Hz
  //MOD0_PORT->ODR ^= MOD0_PIN;

  uint32_t index;

  FM0InterruptCounter++;

  if (FM0InterruptCounter % 2)
  {
    if(FM0Bit & FM0Byte);
      //do nothing
    else
    {
      //toggle for a zero
      MOD0_PORT->ODR ^= MOD0_PIN;
    }
  }
  else
  {
    if (FM0Bit == 1)
    {
      FM0Bit = 0x80;
      FM0Byte = *TXmsgPointer++;
      FM0ByteNumber++;
    }
    else
      FM0Bit >>= 1;                // next bit

      // Always toggle for rising edge
    MOD0_PORT->ODR ^= MOD0_PIN;
  }

  if(FM0ByteNumber > 63)
  {
    FM0ByteNumber = 0;

    index = TXmsgNumber % 4;

    TXmsgNumber++;
    TXmsgPointer = &TXmsgOut[index].Byte[0];

    // Let the main know we need a new packet built (since buffered,
    // we will have 250ms)
    Flag_Build_TASK = SET;
  }
} // Modulation_TASK_Handler

// **************************************************************************
//
//  FUNCTION  : Modulation_iMet1_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : This routine should be called each time iMet-1 baud rate
//              interrupt occurs.
//
//  UPDATED   : 2016-07-21 JHM
//
// **************************************************************************
void Modulation_iMet1_Handler(void)
{
  // Uncomment this for baud clock testing
  //MOD0_PORT->ODR ^= MOD0_PIN;
  /*TIM_CCxCmd(TIM4, TIM_Channel_2, DISABLE);
  CCR = TIM_GetCounter(IMET1_TIM);
  if(CCR > Baud_Capture)
  {
	  //TIM_SetCompare2(TIM4, (CCR - Baud_Capture + 0x16));
  }
  TIM_CCxCmd(TIM4, TIM_Channel_2, ENABLE);*/

  if (iMet1Modulation.BitCounter == 0)
  {
	//Check if data is ready to send
    if (iMet1Modulation.Start == iMet1Modulation.End)
    {
      // Buffer is empty. Keep CCR value at NULL
      iMet1Modulation.CCR_Value = IMET1_NULL_CCR_VAL;
    }
    else
    {
    	if(GPIO_ReadOutputDataBit(MOD0_PORT, MOD0_PIN) == Bit_RESET)
    	{
    		//There is data to send. Determine if its a 1 or 0
    		if(iMet1Modulation.Buffer[iMet1Modulation.Start] & 1)
    		{
    			iMet1Modulation.CCR_Value = IMET1_MARK_CCR_VAL; //Send a mark (1)
    		}

    		else
    		{
    			iMet1Modulation.CCR_Value = IMET1_SPACE_CCR_VAL; //Send a Space (0)
    		}
    		// Increment the bitcounter
    		iMet1Modulation.BitCounter = 1;
    	}
     }
  }
  else
  {
    if (iMet1Modulation.BitCounter < 8)
    {
    	//Determine if data to send is a one or a zero.
    	if(iMet1Modulation.Buffer[iMet1Modulation.Start] & (0x01 << iMet1Modulation.BitCounter))
    	{
    		iMet1Modulation.CCR_Value = IMET1_MARK_CCR_VAL; //Send a Mark (1)
    	}
    	else
    	{
    		iMet1Modulation.CCR_Value = IMET1_SPACE_CCR_VAL; //Send a Space (0)
    	}
    	iMet1Modulation.BitCounter++;
    }
    else
    {
    	//End of the data byte. Send a null
    	if(iMet1Modulation.BitCounter < 9)
    	{
    		iMet1Modulation.CCR_Value = IMET1_NULL_CCR_VAL;
    		iMet1Modulation.BitCounter++;
    	}
    	//Start over again and increment the data buffer
    	else
    	{
    		iMet1Modulation.Start = (iMet1Modulation.Start + 1) % IMET1_BUF_SIZE;
    		iMet1Modulation.BitCounter = 0;
    	}
	}
   }


} // Modulation_iMet1_Handler

// **************************************************************************
//
//  FUNCTION  : Modulation_iMet1_Send
//
//  I/P       : uint8_t* Bytes = Pointer to the byte array to send
//              uint16_t Length = Number of bytes
//
//  O/P       : None.
//
//  OPERATION : Writes data to the modulation buffer. This will automatically
//              be handled by the interrupts and send out the data pin.
//
//  UPDATED   : 2016-07-21 JHM
//
// **************************************************************************
void Modulation_iMet1_Send(uint8_t* Bytes, uint16_t Length)
{
  int i;
  uint8_t n, checksum = 0;

  //Find number of DLEs in the data packet
  for (i = 0; i < Length; i++)
  {
	  if(Bytes[i] == DLE) {n++;}
  }

  if((Length > 0) && (Tx_Buffer_SpaceAvail() >= Length + n + PRELUDE_BYTE_COUNT + POSTLUDE_BYTE_COUNT + 5))
  {
	  //Place the Prelude Data in the Buffer
	  for(i = 0; i < PRELUDE_BYTE_COUNT; i++)
	  {
		  iMet1Modulation.Buffer[iMet1Modulation.End] = PRELUDE_BYTE;
		  iMet1Modulation.End = (iMet1Modulation.End + 1) % IMET1_PTUX_SIZE;
		  //Mfg_SendByte(PRELUDE_BYTE);
	  }

	  //Start the message with the DLE byte
	  iMet1Modulation.Buffer[iMet1Modulation.End] = DLE;
	  //Mfg_SendByte(DLE);

	  //Update the Checksum
	  checksum ^= DLE;
	  iMet1Modulation.End = (iMet1Modulation.End + 1) % IMET1_PTUX_SIZE;

	  //Insert Packet Size
	  iMet1Modulation.Buffer[iMet1Modulation.End] = Length;
	  iMet1Modulation.End = (iMet1Modulation.End + 1) % IMET1_PTUX_SIZE;
	  //Mfg_SendByte(Length);
	  checksum ^= Length;

	  //Insert the Packet Data Now
	  for(i = 0; i < Length; i++)
	  {
		  iMet1Modulation.Buffer[iMet1Modulation.End] = Bytes[i];
		  //Update the checksum
		  checksum ^= Bytes[i];
		  iMet1Modulation.End = (iMet1Modulation.End + 1) % IMET1_PTUX_SIZE;
		  //Mfg_SendByte(Bytes[i]);
		  //Check for DLE and insert another
		  if(Bytes[i] == DLE)
		  {
			  iMet1Modulation.Buffer[iMet1Modulation.End] = DLE;
			  iMet1Modulation.End = (iMet1Modulation.End + 1) % IMET1_PTUX_SIZE;
			  //Mfg_SendByte(Bytes[i]);
		  }
	  }

	  //End of message, terminate with DLE and ETX
	  iMet1Modulation.Buffer[iMet1Modulation.End] = DLE;
	  //Mfg_SendByte(DLE);
	  checksum ^= DLE;
	  iMet1Modulation.End = (iMet1Modulation.End + 1) % IMET1_PTUX_SIZE;
	  iMet1Modulation.Buffer[iMet1Modulation.End] = ETX;
	  checksum ^= ETX;
	  //Mfg_SendByte(ETX);
	  iMet1Modulation.End = (iMet1Modulation.End + 1) % IMET1_PTUX_SIZE;

	  //Deal with potential problems with the checksum
	  if ((checksum == ETX) || (checksum == DLE)) {checksum++;}

	  //Add Checksum to the Buffer
	  iMet1Modulation.Buffer[iMet1Modulation.End] = checksum;
	  //Mfg_SendByte(checksum);
	  iMet1Modulation.End = (iMet1Modulation.End + 1) % IMET1_PTUX_SIZE;

	  //Put the Postlude Data in the Buffer
	  for (i = 0; i < POSTLUDE_BYTE_COUNT; i++)
	  {
		  iMet1Modulation.Buffer[iMet1Modulation.End] = POSTLUDE_BYTE;
		  //Mfg_SendByte(POSTLUDE_BYTE);
		  iMet1Modulation.End = (iMet1Modulation.End + 1) % IMET1_PTUX_SIZE;
	  }
  }
} // Modulation_iMet1_Send

// **************************************************************************
//
//  FUNCTION  : Tx_Buffer_SpaceAvail
//
//  I/P       : None.
//
//  O/P       : Amount of room in the TX Buffer.
//
//  OPERATION : checks the amount room left in the buffer
//
//  UPDATED   : 2019-10-01 BRH
//
// **************************************************************************
int Tx_Buffer_SpaceAvail()
{
    uint16_t l;

    l = iMet1Modulation.End;
    if (l < iMet1Modulation.Start) {                    // deal with buffer wrap
        l += IMET1_PTUX_SIZE;
    }
    return IMET1_PTUX_SIZE - (l - iMet1Modulation.Start) - 1;   // return space left
}//Tx_Buffer_SpaceAvail
