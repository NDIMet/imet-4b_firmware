#ifndef __THERMISTOR_H
#define __THERMISTOR_H
// **************************************************************************
//
//      International Met Systems
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   thermistor.h
//
//      CONTENTS    :   Header file for the PB5 thermistor
//
// **************************************************************************

// *************************************************************************
// CONSTANTS
// *************************************************************************
#define DEFAULT_A0            +1.00836277e-03
#define DEFAULT_A1            +2.61922412e-04
#define DEFAULT_A2            0.0
#define DEFAULT_A3            +1.50125436e-07

// Hardware
#define NTC_REF_PORT          GPIOA
#define NTC_REF_PIN           GPIO_Pin_4
#define NTC_REF_RCC           RCC_AHBPeriph_GPIOA

#define NTC_COMMON_PORT       GPIOA
#define NTC_COMMON_PIN        GPIO_Pin_1
#define NTC_COMMON_RCC        RCC_AHBPeriph_GPIOA

#define NTC_DRIVE_PORT        GPIOA
#define NTC_DRIVE_PIN         GPIO_Pin_0
#define NTC_DRIVE_RCC         RCC_AHBPeriph_GPIOA

#define NTC_ST_OFFLINE        0
#define NTC_ST_START          1
#define NTC_ST_REF_CHG        2
#define NTC_ST_REF_MEAS       3
#define NTC_ST_NTC_CHG        4
#define NTC_ST_NTC_MEAS       5
#define NTC_ST_CALC           6

#define NTC_TIM               TIM5
#define NTC_TIM_RCC           RCC_APB1Periph_TIM5

#define NTC_COMP              COMP_Selection_COMP1

#define NTC_REF_RESISTOR      64900.0
#define NTC_DEFAULT_R17       1007062.0

#define NTC_LO_A2             -2.090288E-07
#define NTC_LO_A1             1.002296E-02
#define NTC_LO_A0             -1.215269E+02

#define NTC_K                 0.1
#define NTC_REF_K             0.1

// *************************************************************************
// TYPES
// *************************************************************************
typedef struct
{
  double A0;
  double A1;
  double A2;
  double A3;
} sSteinhartCoefficients;

typedef struct
{
  uint8_t State;
  FlagStatus Flag_Error;
  uint32_t Reference_Count;
  uint32_t NTC_Count;
  uint32_t Filtered_Reference;
  uint32_t Filtered_NTC;
  double Reference_FilterK;
  double NTC_FilterK;
  double Resistance;
  double EquivalentResistance;
  double CorrectedResistance;
  double ParallelResistance;
  uint16_t TemperatureK;
  int16_t TemperatureC;
  // Steinhart-Hart coefficients
  sSteinhartCoefficients Coefficients;
} sThermistor;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
extern sThermistor Thermistor;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
void NTC_Init(sThermistor* ThermistorStructure);
void NTC_Handler(sThermistor* ThermistorStructure);
void NTC_CalibrateParallelR(sThermistor* ThermistorStructure);

#endif
