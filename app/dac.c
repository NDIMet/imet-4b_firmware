// **************************************************************************
//
//      Developed by Safari Circuits for International Met Systems
//
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Billy Hopkins
//
//                  Safari Circuits
//					411 Washington Street
//                  Otsego, MI 49078
//					Ph : (269) 694-9471 x337
//
//					Intermet Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//                  Ph : (616) 285-7810
//
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   dac.c
//
//      CONTENTS    :   Routines for DAC setup
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        LOCAL CONSTANTS
//
// **************************************************************************
//None.

// *************************************************************************
//        LOCAL TYPES
// *************************************************************************
//None.

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
//None.

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
//None.

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
//None.

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************


// **************************************************************************
//
//  FUNCTION  : DAC_Initialize
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the DAC pins and sets them to the desired output voltage
//
//  UPDATED   : 2019-08-15  BRH
//
// **************************************************************************
void DAC_Initialize(void)
{
	//Initialize the DAC and GPIO Structures
	DAC_InitTypeDef DAC_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;

	//Enable the DAC and GPIO Clocks
	RCC_APB1PeriphClockCmd(DAC1_RCC, ENABLE);
	RCC_APB1PeriphClockCmd(DAC2_RCC, ENABLE);
	RCC_AHBPeriphClockCmd(DAC_GPIO_RCC, ENABLE);

	//Setup up PA5 (DAC1 Channel 2)
	GPIO_InitStructure.GPIO_Pin = DAC1_MODL_PIN;
	GPIO_InitStructure.GPIO_Mode = DAC1_MODL_MODE;
	GPIO_InitStructure.GPIO_PuPd = DAC1_MODL_PUPD;
	GPIO_Init(DAC1_MODL_PORT, &GPIO_InitStructure);

	//Setup up PA5 (DAC1 Channel 2)
	GPIO_InitStructure.GPIO_Pin = DAC2_MODH_PIN;
	GPIO_InitStructure.GPIO_Mode = DAC2_MODH_MODE;
	GPIO_InitStructure.GPIO_PuPd = DAC2_MODH_PUPD;
	GPIO_Init(DAC2_MODH_PORT, &GPIO_InitStructure);

	//Setup DAC to Pins
	DAC_InitStructure.DAC_Trigger = DAC_TRIGGER;
	DAC_InitStructure.DAC_WaveGeneration = DAC_WAVEGEN;
	DAC_InitStructure.DAC_OutputBuffer = DAC_OUTBUFF;
	DAC_Init(DAC1_NUMBER, DAC1_CHANNEL, &DAC_InitStructure);
	DAC_Cmd(DAC1_NUMBER, DAC1_CHANNEL, ENABLE);
	DAC_Init(DAC2_NUMBER, DAC2_CHANNEL, &DAC_InitStructure);
	DAC_Cmd(DAC2_NUMBER, DAC2_CHANNEL, ENABLE);

	//Set the Output voltage
	//For 0.4V Output, with 3.3V input, 12bit FS output:
	//(0.4/3.3)*4095 = 496.3
	//1V = 1241
	//0.8V = 993
	DAC_SetChannel2Data(DAC1_NUMBER, DAC_Align_12b_R, 1493);
	//For 2.5V Output, with 3.3V input, 12bit FS output:
	//(2.5/3.3)*4095 = 3102.2
	//1.5V = 1861
	//1.9V = 2358
	DAC_SetChannel1Data(DAC2_NUMBER, DAC_Align_12b_R, 1791);


}

