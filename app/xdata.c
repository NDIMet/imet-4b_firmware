// **************************************************************************
//
//      International Met Systems
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   xdata.c
//
//      CONTENTS    :   Routines for implementing xdata protocol
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        LOCAL CONSTANTS
//
// **************************************************************************
// None.

// *************************************************************************
//        LOCAL TYPES
// *************************************************************************
// None.

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
sXDATA xData;

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
// None.

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void ASCIItoBytes(uint8_t* destination, char* source, uint8_t length);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : ASCIItoBytes
//
//  I/P       : uint8_t* destination = Destination byte array
//                      char* source = Source ASCII string
//                    uint8_t length = Length of string
//
//  O/P       : None.
//
//  OPERATION : Converts the ASCII byte representation into the destination
//              byte array.
//
//  UPDATED   : 2018-02-27 JHM
//
// **************************************************************************
static void ASCIItoBytes(uint8_t* destination, char* source, uint8_t length)
{
  int i, j;
  uint8_t bytes[length];
  uint8_t byte;
  char *ptr = source;

  for (i = 0, j = 0; i < length; i++, ptr++)
  {
    if ((*ptr >= '0') && (*ptr <= '9'))
    {
      // Convert ASCII number to hex
      byte = *ptr - '0';
      //Mfg_TransmitMessage("Number");
    }
    else if ((*ptr  >= 'A') && (*ptr  <= 'F'))
    {
      byte = (*ptr  - 'A' + 10);
      //Mfg_TransmitMessage("Letter");
    }
    else
    {
      xData.Flag_Error = SET;
      return;
    }

    if (i % 2)
    {
      // Odd
      bytes[j] |= byte;
      j++;

    }
    else
    {
      // Even
      bytes[j] = (byte << 4);
    }
  } // for

  memcpy(destination, &bytes, sizeof(bytes));

} // ASCIItoBytes

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : XDATA_Init
//
//  I/P       : sXDATA* xd = Pointer to XDATA structure
//
//  O/P       : None.
//
//  OPERATION : Initializes the XDATA structure
//
//  UPDATED   : 2017-06-23 JHM
//
// **************************************************************************
void XDATA_Init(sXDATA* xd)
{
  int i, j;

  xd->Start = 0;
  xd->End = 0;
  xd->Flag_Error = RESET;
  xd->Flag_XOFF = RESET;
  xd->BytesLeft = XDATA_MAX_BYTES;

  for (i = 0; i < XDATA_BUF_SIZE; i++)
  {
    xd->Packets->PayloadSize = 0;
    xd->Packets->PacketSize = 0;
    for (j = 0; j < XDATA_MAX_BYTES; j++)
    {
      xd->Packets[i].Packet[j] = 0;
    } // for
  } // for
} // XDATA_Init

// **************************************************************************
//
//  FUNCTION  : XDATA_Build
//
//  I/P       : sXDATA* xd = Pointer to XDATA structure
//             char* chars = ASCII data from the manufacturing port in hex
//                           format
//             uint8_t length = length of the character array
//
//  O/P       : None.
//
//  OPERATION : Puts the XDATA into a packet and sets the flag to send the
//              data out the transmitter.
//
//  UPDATED   : 2018-02-27 JHM
//
// **************************************************************************
void XDATA_Build(sXDATA* xd, char* chars)
{
  char message[XDATA_MAX_CHARS];
  uint8_t ascii_length;
  uint8_t payload_size;
  uint8_t buffer_space;
  uint16_t Checksum;
  int i;

  if (xd->Flag_XOFF == SET)
  {
    Mfg_SendByte(XOFF);
    return;
  }

  for (i = 0; i < XDATA_MAX_CHARS; i++)
  {
    if (chars[i] == '\r')
    {
      message[i] = 0;
      break;
    }
    else if (chars[i] == '\0')
    {
      // An error has occurred, since the string has ended without a carriage return
      xd->Flag_Error = SET;
      return;
    }
    else
    {
      message[i] = chars[i];
    }
  }

  if (i == XDATA_MAX_CHARS)
  {
    xd->Flag_Error = SET;
    return;
  }

  // Determine the packet length sent by the user
  ascii_length = strlen(message);
  payload_size = ascii_length / 2;

  // Set the payload size in the structure
  xd->Packets[xd->End].PayloadSize = payload_size;
  // Set the packet size in the structure
  xd->Packets[xd->End].PacketSize = payload_size + 5;

  // Add the preamble
  xd->Packets[xd->End].Packet[0] = XDATA_SOH;
  xd->Packets[xd->End].Packet[1] = XDATA_PKT_ID;
  xd->Packets[xd->End].Packet[2] = payload_size;

  // Convert the ASCII hex data to binary
  ASCIItoBytes(&xd->Packets[xd->End].Packet[3], message, ascii_length);

  // Clear the checksums
  xd->Packets[xd->End].Packet[payload_size + 3] = 0;
  xd->Packets[xd->End].Packet[payload_size + 4] = 0;

  // Calculate the checksum
  Checksum = calc_crc(xd->Packets[xd->End].Packet, 0, xd->Packets[xd->End].PacketSize, 0);
  // Checksum MSB
  xd->Packets[xd->End].Packet[payload_size + 3] = (uint8_t)(Checksum >> 8);
  // Checksum LSB
  xd->Packets[xd->End].Packet[payload_size + 4] = (uint8_t)(Checksum & 0xFF);

  // Notify the user
  sprintUnsignedNumber(variable, payload_size, 2);
  strcpy(message, variable);
  StrCat(message, " sent.\r\n");

  Mfg_TransmitMessage(message);

  // Increment the end of the buffer
  xd->End = (xd->End + 1) % XDATA_BUF_SIZE;

  buffer_space = XDATA_GetBufferSpace(xd);

  if (buffer_space <= 1)
  {
    xd->Flag_XOFF = SET;
    Mfg_SendByte(XOFF);
  }
} // XDATA_Send

// **************************************************************************
//
//  FUNCTION  : XDATA_Handler
//
//  I/P       : sXDATA* xd = Pointer to XDATA structure
//
//  O/P       : uint8_t = Slots remaining in the xdata packet buffer
//
//  OPERATION :
//
//  UPDATED   : 2018-03-22 JHM
//
// **************************************************************************
uint8_t XDATA_GetBufferSpace(sXDATA* xd)
{
  int bufferspace = xd->End - xd->Start;

  if (bufferspace < 0)
  {
    bufferspace = XDATA_BUF_SIZE + bufferspace;
  }

  bufferspace = XDATA_BUF_SIZE - bufferspace;

  return (uint8_t)bufferspace;
} // XDATA_Handler
