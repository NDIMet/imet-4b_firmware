#ifndef __dac_h__
#define __dac_h__
// **************************************************************************
//
//      Developed by Safari Circuits for International Met Systems
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Billy Hopkins
//
//                  Safari Circuits
//					411 Washington Street
//                  Otsego, MI 49078
//					Ph : (269) 694-9471 x337
//
//					Intermet Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//                  Ph : (616) 285-7810
//
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   dac.h
//
//      CONTENTS    :   Header file for the DAC
//
// **************************************************************************
// *************************************************************************
// INCLUDES
// *************************************************************************
#include "includes.h"

// *************************************************************************
// CONSTANTS
// *************************************************************************

//Hardware
#define DAC_GPIO_RCC	RCC_AHBPeriph_GPIOA

#define DAC1_MODL_PORT	GPIOA
#define DAC1_MODL_PIN	GPIO_Pin_5
#define DAC1_MODL_MODE	GPIO_Mode_AN
#define DAC1_MODL_PUPD	GPIO_PuPd_NOPULL
#define DAC1_CHANNEL	DAC_Channel_2
#define DAC1_NUMBER		DAC1

#define DAC2_MODH_PORT 	GPIOA
#define DAC2_MODH_PIN	GPIO_Pin_6
#define DAC2_MODH_MODE	GPIO_Mode_AN
#define DAC2_MODH_PUPD	GPIO_PuPd_NOPULL
#define DAC2_CHANNEL	DAC_Channel_1
#define DAC2_NUMBER		DAC2

#define DAC1_RCC		RCC_APB1Periph_DAC1
#define DAC2_RCC		RCC_APB1Periph_DAC2
#define DAC_TRIGGER		DAC_Trigger_None
#define DAC_WAVEGEN		DAC_WaveGeneration_None
#define DAC_OUTBUFF		DAC_OutputBuffer_Enable


// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
void DAC_Initialize(void);

#endif
