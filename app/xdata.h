#ifndef __XDATA_H
#define __XDATA_H
// **************************************************************************
//
//      International Met Systems
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   xdata.h
//
//      CONTENTS    :   Header file for the xdata protocol
//
// **************************************************************************

// *************************************************************************
// CONSTANTS
// *************************************************************************
#define XDATA_BUF_SIZE        10
#define XDATA_MAX_CHARS       101
#define XDATA_MAX_BYTES       80

#define XDATA_SOH             0x01
#define XDATA_PKT_ID          0x03

#define XOFF                  0x13
#define XON                   0x11

// *************************************************************************
// TYPES
// *************************************************************************
typedef struct
{
  uint8_t PayloadSize;
  uint8_t PacketSize;
  uint8_t Packet[XDATA_MAX_BYTES];
} sXDATA_Packet;

typedef struct
{
  uint8_t Start;
  uint8_t End;
  sXDATA_Packet Packets[XDATA_BUF_SIZE];
  FlagStatus Flag_Error;
  FlagStatus Flag_XOFF;
  int16_t BytesLeft;
} sXDATA;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
extern sXDATA xData;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
void XDATA_Init(sXDATA* xd);
void XDATA_Build(sXDATA* xd, char* chars);
uint8_t XDATA_GetBufferSpace(sXDATA* xd);

#endif
