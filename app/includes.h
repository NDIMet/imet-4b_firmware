#ifndef __INCLUDES_H
#define __INCLUDES_H
// **************************************************************************
//
//      International Met Systems
//
//      iQ Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// Generic C libraries
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

// Processor Specific
#include <system_stm32f37x.h>
#include <stm32f37x_conf.h>

// Application Files
#include "GRF5010.h"
#include "user.h"
#include "peripherals.h"
#include "mfg.h"
#include "utilities.h"
#include "MS5607.h"
#include "HYT271.h"
#include "defroster.h"
#include "thermistor.h"
#include "battery.h"
#include "hvac.h"
#include "ublox.h"
#include "modulation.h"
#include "crc.h"
#include "packets.h"
#include "dac.h"
#include "flight.h"
#include "config.h"
#include "xdata.h"


// Constants
#define FALSE   0
#define TRUE    ~FALSE
#define DLE  0x10
#define ETX  0x03

extern char string[100];
extern char variable[20];
extern char FirmwareVersion[];
extern char SoftwareNumber[];
extern uint8_t Initialized; // added 05/07/21 to handle PB initialization interrupt

volatile uint32_t i,j;

#endif /* __INCLUDES_H */
