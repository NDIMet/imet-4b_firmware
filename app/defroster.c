// **************************************************************************
//
//      International Met Systems
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   DEFROSTER.C
//
//      CONTENTS    :   Routines for the defrost circuit
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        LOCAL CONSTANTS
//
// **************************************************************************
// None

// *************************************************************************
//        LOCAL TYPES
// *************************************************************************
// None.

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
sDEFROSTER Defroster;

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
// None.

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void InitGPIO(void);
static void BenoitAlgorithm(void);
static void MeulenbergAlgorithm(void);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : InitGPIO
//
//  I/P       : None
//
//  O/P       : None.
//
//  OPERATION : Initializes the GPIO for the defroster pin.
//
//  UPDATED   : 2017-04-07 JHM
//
// **************************************************************************
static void InitGPIO(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // Enable the clock
  RCC_AHBPeriphClockCmd(DF_RCC, ENABLE);

  // Configure the pin for output with pull-up
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Pin = DF_PIN;
  GPIO_Init(DF_PORT, &GPIO_InitStructure);

  // Set the initial state to off
  GPIO_ResetBits(DF_PORT, DF_PIN);
} // InitGPIO

// **************************************************************************
//
//  FUNCTION  : BenoitAlgorithm
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Turns the defroster on or off based on Mark's algorithm. I'm
//              starting to think Mark is smarter than I originally thought.
//              Not only is this algorithm more scientifically sound...it 
//              actually works too!
//
//  UPDATED   : 2017-04-13
//
// **************************************************************************
static void BenoitAlgorithm(void)
{
  float Dewpoint;
  float Frostpoint;
  float AirTemp;
  float Humidity;
  float HumidityTemp;
  float SaturationVapourPressure;
  float VapourPressure;

  if (Thermistor.State == NTC_ST_OFFLINE) return;
  if (HumiditySensor.State == HYT271_ST_OFFLINE) return;

  // Calculate the AirTemp in C and the RH
  AirTemp = (float)Thermistor.TemperatureC / 100.0;
  Humidity = (float)HumiditySensor.RawHumidity / 10.0;
  HumidityTemp = (float)HumiditySensor.CorrectedTemperature / 100.0;

  // Calculate the dewpoint
  Dewpoint = AirTemp - (100.0 - Humidity) / 5.0;

  // Calculate the frostpoint
  Frostpoint = Dewpoint / 1.1;

  // Calculate the saturation vapour pressure (in mbar) using the Clausius
  // Clapeyron Equation
  SaturationVapourPressure = 17.625 * AirTemp;
  SaturationVapourPressure /= AirTemp + 243.04;
  SaturationVapourPressure = exp(SaturationVapourPressure);
  SaturationVapourPressure *= 6.1094;

  // Calculate the vapour pressure
  // e = RH * es
  VapourPressure = Humidity * SaturationVapourPressure * 0.01;

  // Calculate the dewpoint
  Dewpoint = (VapourPressure * 100.0 / 611.0);
  Dewpoint = log(Dewpoint);
  Dewpoint *= 237.3;
  Dewpoint /= (17.269388 - log(VapourPressure * 100.0 / 611.0));

  // Calculate the frostpoint
  Frostpoint = Dewpoint / 1.1;

  if ((Frostpoint >= (AirTemp - DF_THRESHOLD)) && (Frostpoint >= (HumidityTemp - DF_THRESHOLD)))
  {
    // Turn the defroster on
    GPIO_SetBits(DF_PORT, DF_PIN);
  }
  else
  {
    // Turn the defroster off
    GPIO_ResetBits(DF_PORT, DF_PIN);
  }
} // BenoitAlgorithm

// **************************************************************************
//
//  FUNCTION  : MeulenbergAlgorithm
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : An algorithm to prevent icing. It is superior to the Benoit
//              Algorithm in practically every way.
//
//  UPDATED   : 2017-04-07 JHM
//
// **************************************************************************
static void MeulenbergAlgorithm(void)
{
  if (Thermistor.State == NTC_ST_OFFLINE) return;
  if (HumiditySensor.State == HYT271_ST_OFFLINE) return;

  // Turn the heater on if the temperature is above freezing and the humidity
  // is greater than 95%
  if ((HumiditySensor.RawTemperature > -1000) && HumiditySensor.RawHumidity > 950)
  {
    GPIO_SetBits(DF_PORT, DF_PIN);
    Defroster.Flag_DefrosterOn = SET;
  }
  else
  {
    GPIO_ResetBits(DF_PORT, DF_PIN);
    Defroster.Flag_DefrosterOn = RESET;
  }
} // MeulenbergAlgorithm

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : Defroster_Init
//
//  I/P       : sDEFROSTER* ptrDF = Pointer to the defroster structure
//
//  O/P       : None.
//
//  OPERATION : Initializes the values of the defroster
//
//  UPDATED   : 2017-04-07 JHM
//
// **************************************************************************
void Defroster_Init(sDEFROSTER* ptrDF)
{
  // Initialize the GPIO
  InitGPIO();

  // Set the mode to the configuration
  Defroster_SetMode(ptrDF, iQ_Config.Defrost_Mode);
} // Defroster_Init

// **************************************************************************
//
//  FUNCTION  : Defroster_Handler
//
//  I/P       : sDEFROSTER* ptrDF = Pointer to the defroster structure
//
//  O/P       : None.
//
//  OPERATION : Handles the state machine for the defroster circuit.
//
//  UPDATED   : 2017-04-07 JHM
//
// **************************************************************************
void Defroster_Handler(sDEFROSTER* ptrDF)
{
  if (GetWaitFlagStatus(DF_WAIT_CH) == SET)
  {
    return;
  }

  if (ptrDF->Flag_Forced == SET)
  {
    return;
  }

  if (ptrDF->Mode == DF_Mode_Benoit)
  {
    BenoitAlgorithm();
  }
  else if (ptrDF->Mode == DF_Mode_Meulenberg)
  {
    MeulenbergAlgorithm();
  }

  // Wait 1 second
  Wait(DF_WAIT_CH, 1000);
} // Defroster_Handler

// **************************************************************************
//
//  FUNCTION  : Defroster_SetMode
//
//  I/P       : sDEFROSTER* ptrDF = Pointer to the defroster structure
//
//  O/P       : None.
//
//  OPERATION : Sets the mode of the defroster circuit
//
//  UPDATED   : 2017-04-07 JHM
//
// **************************************************************************
void Defroster_SetMode(sDEFROSTER* ptrDF, TypeDef_DF_Mode Mode)
{
  switch (Mode)
  {
    case DF_Mode_Off:
      GPIO_ResetBits(DF_PORT, DF_PIN);
      ptrDF->Flag_Forced = SET;
      ptrDF->Flag_DefrosterOn = RESET;
      break;

    case DF_Mode_On:
      GPIO_SetBits(DF_PORT, DF_PIN);
      ptrDF->Flag_Forced = SET;
      ptrDF->Flag_DefrosterOn = SET;
      break;

    default:
      GPIO_ResetBits(DF_PORT, DF_PIN);
      ptrDF->Flag_Forced = RESET;
      ptrDF->Flag_DefrosterOn = RESET;
      break;
  } // switch

  // Update the data structure
  ptrDF->Mode = Mode;
} // Defroster_SetMode
