// **************************************************************************
//
//      International Met Systems
//
//      iQ Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   utilities.c
//
//      CONTENTS    :   Useful miscellaneous functions
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        CONSTANTS
//
// **************************************************************************
// None.

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
// None.

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************
// None.

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// *************************************************************************
//
//  FUNCTION  : DelayMs
//
//  I/P       : unsigned int uiDelay - The required delay, in ms
//
//  O/P       :
//
//  OPERATION : Empirically tested, using uiDelay = 10, to give a delay
//              approximately equal to uiDelay milliseconds.
//              HCLK = 48MHz
//
//  UPDATED   : 2014-02-07
//
// *************************************************************************
void DelayMs(unsigned int uiDelay)
{
  unsigned int n;

  while (uiDelay-- > 0)
  {
    n = 3160;
    while (n-- > 0);
  }
} // DelayMs

// *************************************************************************
//
//  FUNCTION  :  HexConv
//
//  I/P       :
//
//  O/P       :
//
//  OPERATION : Convert a 2-character ASCII string into its hex value
//
//  UPDATED   : 2009-06-06
//
// *************************************************************************
unsigned char HexConv(char* ucpData)
{
  unsigned char c;

  if ((*ucpData) > '9')
    c = (*ucpData)-'A'+10;
  else
    c = (*ucpData)-'0';

  c <<= 4;

  if ((*(ucpData+1)) > '9')
    c |= ((*(ucpData+1))-'A'+10);
  else
    c |= ((*(ucpData+1))-'0');

  return (c);
} // HexConv

// *************************************************************************
//
//  FUNCTION  : GetNextField
//
//  I/P       : cpData (char *) - Points to a character in a string
//
//  O/P       : Points to the first character in the next field, or to the
//              null-terminator character.
//
//  OPERATION : Point to the first character in the next field in a comma-
//              separated field.   Point to the null-terminator at the end.
//
//  UPDATED   : 2012-09-19
//
// *************************************************************************
char* GetNextField(char* cpData)
{
   while ((*cpData != 0x00) && (*cpData != ','))
     cpData++;
   if (*cpData == ',')
     cpData++;
   return(cpData);
} // GetNextField

// *************************************************************************
//
//  FUNCTION  : ReadInt16
//
//  I/P       : cpData (char *) - Pointer to a character array
//
//  O/P       : 16-bit signed integer (two's compliment)
//
//  OPERATION : Converts a null terminated string into an 16-bit integer.
//              Returns 0 if the operation fails.
//
//  UPDATED   : 2015-08-14 JHM
//
// *************************************************************************
int32_t ReadInt(char* cpData)
{
  int i;
  int Sign;
  int32_t Number;

  Sign = 1;
  Number = 0;

  // Escape if number is null
  if (*cpData == 0)
  {
    return 0;
  }

  // Handle first character and escape if it is invalid
  if (*cpData == '-')
  {
    Sign = -1;
    cpData++;
  }
  else if (*cpData == '+')
  {
    cpData++;
  }

  for (i = 0; i < 10; i++)
  {
    if (*cpData == 0)
    {
      break;
    }
    else if ((*cpData > '9') || (*cpData < '0'))
    {
      return 0;
    }
    else
    {
      Number *= 10;
      Number += (*cpData - '0');
      cpData++;
    }
  } // for

  return Number * Sign;
} // ReadInt

// *************************************************************************
//
//  FUNCTION  : ReadDouble
//
//  I/P       : cpData (char *) - Points to a character in a string
//
//  O/P       : Pointer to character array
//
//  OPERATION : Converts a null terminated string into a floating point
//              number.  Returns 0.0 if the operation fails.
//
//  UPDATED   : 2015-08-14 JHM
//
// *************************************************************************
double ReadDouble(char* cpData)
{
  int i;
  int Sign;
  int32_t LeftSide;
  int32_t RightSide;
  double fNumber;
  double Multiplier;

  Sign = 1;
  LeftSide = 0;
  RightSide = 0;
  fNumber = 0.0;
  Multiplier = 1.0;

  // Escape if number is null
  if (*cpData == 0)
  {
    return 0.0;
  }

  // Handle first character and escape if it is invalid
  if (*cpData == '-')
  {
    Sign = -1;
    cpData++;
  }
  else if (*cpData == '+')
  {
    cpData++;
  }
  else if (*cpData > '9' || *cpData < '0')
  {
    return 0.0;
  }

  // Get the left side
  LeftSide = 0;

  for (i = 0; i < 10; i++)
  {
    if (*cpData == 0)
    {
      return (double)(LeftSide * Sign);
    }
    else if (*cpData == '.')
    {
      cpData++;
      LeftSide *= Sign;
      goto Decimal;
    }
    else if ((*cpData == 'E') || (*cpData == 'e'))
    {
      cpData++;
      LeftSide *= Sign;
      fNumber = (double)LeftSide;
      goto Exponent;
    }
    else if ((*cpData > '9') || (*cpData < '0'))
    {
      return 0.0;
    }
    else
    {
      LeftSide *= 10;
      LeftSide += (*cpData - '0');
      cpData++;
    }
  } // for

Decimal:
  // Get the right side
  RightSide = 0;
  Multiplier = 1.0;

  for (i = 0; i < 10; i++)
  {
    if (*cpData == 0)
    {
      RightSide *= Sign;
      return (double)LeftSide + (double)(RightSide) / Multiplier;
    }
    else if ((*cpData == 'E') || (*cpData == 'e'))
    {
      cpData++;
      RightSide *= Sign;
      fNumber = (double)LeftSide + (double)(RightSide) / Multiplier;
      goto Exponent;
    }
    else if ((*cpData > '9') || (*cpData < '0'))
    {
      return 0.0;
    }
    else
    {
      RightSide *= 10;
      RightSide += (*cpData - '0');
      Multiplier *= 10.0;
      cpData++;
    }
  } // for

  // If E or e is detected
Exponent:
    Multiplier = 1.0;
    LeftSide = 0;
    Sign = 1;

    if (*cpData == '-')
    {
      Sign = -1;
      cpData++;
    } // if
    else if (*cpData == '+')
    {
      cpData++;
    } // else if

    for (i = 0; i < 3; i++)
    {
      if (*cpData == 0)
      {
        break;
      } // if
      else if ((*cpData > '9') || (*cpData < '0'))
      {
        return 0.0;
      }
      else
      {
        LeftSide *= 10;
        LeftSide += (*cpData - '0');
        cpData++;
      } // else
    } // for

    for (i = 0; i < LeftSide; i++)
    {
      if (Sign == 1)
      {
        Multiplier *= 10.0;
      }
      else
      {
        Multiplier *= 0.1;
      }
    }

    fNumber *= Multiplier;

    return (double)fNumber;
} // ReadDouble

// *************************************************************************
//
//  FUNCTION  : sprintUnsignedNumber
//
//  I/P       : cpData (char *) - Points to the first character in
//              a string.
//              Number (uint32) - The number to be printed
//              CharLength (uint8) - The number of displayed characters
//
//  O/P       : None.
//
//  OPERATION : Converts the given number into a string with CharLength digits.
//              It automatically terminates the string with a null character,
//              and it is up to the programmer to provide a cpData string large
//              enough to fit all the digits from CharLength + 1 (for null).
//
//  UPDATED   : 2013-07-09 JHM
//
// *************************************************************************
void sprintUnsignedNumber(char* cpData, uint32_t Number, uint8_t CharLength)
{
  int i;
  uint32_t ModResult, Placeholder;

  Placeholder = (uint32_t)Number;

  for(i = 1; i <= CharLength; i++)
  {
    ModResult = Placeholder % 10;
    cpData[CharLength - i] = (char)(ModResult) + '0';
    Placeholder -= ModResult;
    Placeholder /= 10;
  }

  cpData[CharLength] = '\0';
}

// *************************************************************************
//
//  FUNCTION  : sprintSignedNumber
//
//  I/P       : cpData (char *) - Points to the first character in
//              a string.
//              Number (int32) - The number to be printed
//              CharLength (uint8) - The number of displayed characters
//
//  O/P       : None.
//
//  OPERATION : Converts the given number into a string with CharLength digits.
//              It automatically terminates the string with a null character
//              and adds the sign at the start of the string. It is up to the
//              programmer to provide a cpData string large enough to fit the
//              digits from CharLength + 2 (for sign and null).
//
//  UPDATED   : 2013-07-09 JHM
//
// *************************************************************************
void sprintSignedNumber(char* cpData, int32_t Number, uint8_t CharLength)
{
  int i;
  uint32_t ModResult, Placeholder;

  // Get the sign
  if(Number < 0)
  {
    cpData[0] = '-';
    Placeholder = (uint32_t)(Number * -1);
  }
  else
  {
    cpData[0]= '+';
    Placeholder = (uint32_t)Number;
  }

  for(i = 0; i < CharLength; i++)
  {
    ModResult = Placeholder % 10;
    cpData[CharLength - i] = (char)(ModResult) + '0';
    Placeholder -= ModResult;
    Placeholder /= 10;
  }

  cpData[CharLength + 1] = '\0';
} // sprintSignedNumber

// *************************************************************************
//
//  FUNCTION  : sprintScientific
//
//  I/P       : cpData (char *) - Points to the first character in
//              a string.
//              Number (float) - The number to be printed
//              CharLength (uint8) - The number of displayed characters
//
//  O/P       : None.
//
//  OPERATION : Converts the given number into a string
//
//  UPDATED   : 2015-06-24 JHM
//
// *************************************************************************
void sprintScientific(char* cpData, double Value, uint8_t DecimalPlaces)
{
  double Calc;
  int Sign;
  uint8_t Power;
  uint8_t LeftSide;
  uint32_t RightSide;

  if (Value == 0.0)
  {
    strcpy(cpData, "0");
    return;
  }

  if (Value >= 0)
  {
    Sign = 1;
  }
  else
  {
    Sign = -1;
  }
  // Get absolute value
  Calc = Value * (double)Sign;

  if (Calc > 1.0)
  {
    for (Power = 0; Calc > 10.0; Power++)
    {
      Calc /= 10.0;
    }

    LeftSide = (uint8_t)(Calc);
    Calc = Calc - (double)LeftSide;
    RightSide = (uint32_t)round((Calc * pow(10, DecimalPlaces)));

    sprintSignedNumber(cpData, (int)LeftSide * Sign, 1);
    cpData += 2; // Get past sign and digit
    strcat(cpData, ".");
    cpData++;
    sprintUnsignedNumber(cpData, RightSide, DecimalPlaces);
    cpData += DecimalPlaces;
    strcat(cpData, "e+");
    cpData += 2;
    sprintUnsignedNumber(cpData, Power, 2);
    cpData += 2;
    *cpData = 0;
  }
  else
  {
    for (Power = 0; Calc < 1.0; Power++)
    {
      Calc *= 10.0;
    }
    LeftSide = (uint8_t)(Calc);
    Calc = Calc - (double)LeftSide;
    RightSide = (uint32_t)round((Calc * pow(10, DecimalPlaces)));

    sprintSignedNumber(cpData, (int)LeftSide * Sign, 1);
    cpData += 2; // Get past sign and digit
    strcat(cpData, ".");
    cpData++;
    sprintUnsignedNumber(cpData, RightSide, DecimalPlaces);
    cpData += DecimalPlaces;
    strcat(cpData, "e-");
    cpData += 2;
    sprintUnsignedNumber(cpData, Power, 2);
    cpData += 2;
    *cpData = 0;
  }

  // Test if the number is greater than 10^1
} // sprintScientific

// *************************************************************************
//
//  FUNCTION  : sprintHex
//
//  I/P       : cpData (char *) - Points to the first character in
//              a string.
//              Number (uint32) - The number to be printed
//              CharLength (uint8) - The number of displayed characters
//
//  O/P       : None.
//
//  OPERATION : Converts the given number into a hex string with CharLength digits.
//              It automatically terminates the string with a null character,
//              and it is up to the programmer to provide a cpData string large
//              enough to fit all the digits from CharLength + 1 (for null).
//
//  UPDATED   : 2013-07-09 JHM
//
// *************************************************************************
void sprintHex(char* cpData, uint32_t Value, uint8_t CharLength)
{
  int i;
  uint32_t Placeholder;

  Placeholder = (uint32_t)Value;

  for(i = 1; i <= CharLength; i++)
  {
    Placeholder = Value >> ((CharLength - i) * 4);
    Placeholder &= 0xF;
    if(Placeholder < 10)
    {
      cpData[i - 1] = Placeholder + '0';
    }
    else
    {
      cpData[i - 1] = Placeholder + 55;
    }
  }

  // Terminate the string
  cpData[CharLength] = '\0';
} // sprintHex

// *************************************************************************
//
//  FUNCTION  : StrCat
//
//  I/P       : char* destination = pointer to destination array
//              char* source = pointer to array to be added to destination
//
//  O/P       : None.
//
//  OPERATION : Replaces the strcat standard c routine from <string.h>.
//
//  UPDATED   : 2016-09-09 JHM
//
// *************************************************************************
void StrCat(char* destination, char* source)
{
  // Get to the end of destination
  while (*destination)
  {
    destination++;
  }

  // Add the source data to the end of the destination
  while (*source)
  {
    *destination = *source;
    destination++;
    source++;
  }

  // Terminate the destination properly
  *destination = 0;
} // StrCat

// *************************************************************************
//
//  FUNCTION  : StrCat
//
//  I/P       : char* cpData = pointer to ASCII data array
//
//  O/P       : None.
//
//  OPERATION : Replaces the strupr standard c routine from <string.h>.
//
//  UPDATED   : 2016-09-09 JHM
//
// *************************************************************************
void StrUpr(char* cpData)
{
 while (*cpData)
 {
   if (*cpData > 96)
   {
     if (*cpData < 123)
     {
       *cpData -= 32;
     }
   }
   cpData++;
 } // while
} // StrUpr

// *************************************************************************
//
//  FUNCTION  : CalculateAverage
//
//  I/P       : float* Array = pointer to an array of floating point numbers
//              uint16_t Count = array size
//
//  O/P       : None.
//
//  OPERATION : Calculates the average.
//
//  UPDATED   : 2017-06-17 JHM
//
// *************************************************************************
double CalculateAverage(uint32_t* Array, uint16_t Count)
{
  int i;
  double Average = 0.0;

  for (i = 0; i < Count; i++)
  {
    Average += (double)Array[i];
  } // for

  Average /= (double)Count;

  return Average;
} // CalculateAverage

