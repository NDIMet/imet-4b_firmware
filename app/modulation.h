#ifndef __MODULATION_H
#define __MODULATION_H
// **************************************************************************
//
//      International Met Systems
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   modulation.h
//
//      CONTENTS    :   Header file for modulating the data to the
//                      transmitter
//
// **************************************************************************
// *************************************************************************
// INCLUDES
// *************************************************************************
#include "includes.h"

// *************************************************************************
// CONSTANTS
// *************************************************************************
// TASK Modulation info
#define FM0_CLK              4096

// Modulation Output Pin
#define MOD0_PORT            GPIOA
#define MOD0_PIN             GPIO_Pin_12
#define MOD0_GPIO_RCC        RCC_AHBPeriph_GPIOA

// Modulation Timer Hardware
#define MOD0_TIM             TIM2
#define MOD0_TIM_RCC         RCC_APB1Periph_TIM2

// iMet-1 Modulation Timer

#define IMET1_TIM            TIM3
#define IMET1_TIM_RCC        RCC_APB1Periph_TIM3
#define IMET1_TIM_CLK        480000
#define IMET1_BUF_SIZE       300

// IMS3010 Frequencies
#define IMET1_MARKF        4800
#define IMET1_SPACEF       2400
#define IMET1_NULLF		 1200
#define IMET1_BAUD         2400

//Baud_CCR = CLOCK/BAUD = 48000/240
#define IMET1_BAUD_CCR_VAL   200
//MARK_CCR = (CLOCK/MARKF)/2 = (48000/4800)/2
#define IMET1_MARK_CCR_VAL   50
//SPACE_CCR = (CLOCK/SPACEF)/2 = (48000/2400)/2
#define IMET1_SPACE_CCR_VAL  100
//NULL_CCR = (CLOCK/NULLF)/2 = (48000/1200)/2
#define IMET1_NULL_CCR_VAL	 200


#define PRELUDE_BYTE_COUNT  5
#define POSTLUDE_BYTE_COUNT 5
#define PRELUDE_BYTE        0x69
#define POSTLUDE_BYTE       0x96

// *************************************************************************
// TYPES
// *************************************************************************
typedef enum
{
  MODULATION_MODE_IMS = 0,
} TypeDef_ModulationMode;

typedef struct
{
  uint16_t Start;
  uint16_t End;
  uint8_t Buffer[IMET1_BUF_SIZE];
  uint16_t CCR_Value;
  uint8_t BitCounter;
} siMet1Modulation;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
extern FlagStatus Flag_Build_TASK;
extern TypeDef_ModulationMode ModulationMode;
extern siMet1Modulation iMet1Modulation;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
void Modulation_Init(void);
void Modulation_Enable(void);
void Modulation_Disable(void);
void Modulation_TASK_Handler(void);
void Modulation_iMet1_Handler(void);
void Modulation_iMet1_Send(uint8_t* Bytes, uint16_t Length);

#endif
